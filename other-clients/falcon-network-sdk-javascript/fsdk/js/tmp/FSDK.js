
export default class FSDK {
    constructor() {
        if (!FSDK.instance) {
            FSDK.instance = this;
        }

        return FSDK.instance;
    }

    static getInstance() {
        if (!FSDK.instance) {
            FSDK.instance = new FSDK();
        }
        FSDK.instance.messages = new Set();
        FSDK.instance.loadClasses();
        return FSDK.instance;
    }

    addClient(fClient) {
        if (!this.clients)
            this.clients = [];
        this.clients.push(fClient);
        this.messages.forEach(fMessage => {
            fClient.register(fMessage);
        })
    }

    start() {
        this.clients.forEach(fClient => {
            fClient.start();
        })
    }

    loadClasses() {
        var r = require.context('./', true, /\.js$/);
        r.keys().forEach(r);
    }

    register(fMessage) {
        this.messages.add(fMessage);
        if (this.clients)
            this.clients.forEach(fClient => {
                fClient.register(fMessage);
            })
    }

    send(fClient, fMessage){
        fClient.send(fMessage);
    }

    send(fMessage){
        if (this.clients)
        this.clients.forEach(fClient => {
            fClient.send(fMessage);
        })
    }
}
