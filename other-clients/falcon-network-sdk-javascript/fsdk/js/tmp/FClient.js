import FCSInitSession from "./FCSInitSession";
import io from 'socket.io-client';
export default class FClient {
    constructor(ip, port, context, ssl) {
        this.ip = ip;
        this.port = port;
        this.context = context;
        this.ssl = ssl;
        this.messages = new Set();
        this.csSequence = 0;
    }

    start() {
        if (this.ssl)
            this.ws = io(`wss://${this.ip}:${this.port}`, {
                path: `/${this.context}`,
                transports: ['websocket']
            });
        else
            this.ws = io(`ws://${this.ip}:${this.port}`, {
                path: `/${this.context}`,
                transports: ['websocket']
            });

        this.ws.on('connect', () => {
            var session = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
            this.send(new FCSInitSession(session));
        });

        this.messages.forEach(fMessage => {
            this.ws.on(fMessage.event, data => {
                fMessage.onData(this, data);
            })
        })

    }

    send(message) {
        this.csSequence++;
        message.csSequence = this.csSequence;
        this.ws.emit(message.event, message);
    }

    register(fMessage) {
        this.messages.add(fMessage);
        if (this.ws)
            this.ws.on(fMessage.event, data => {
                fMessage.onData(data);
            })
    }
}
