import FSessionManager from "./FSessionManager"
import CSInitSession from "../message/CSInitSession"
import FPing from "../message/FPing"
import LogUtil from "../util/LogUtil"
import Util from "../util/Util"
import SCCloseSession from "../message/SCCloseSession"

export default class FSession {
    constructor(channel) {
        this.channel = channel;
        this.channel.session = this;
        this.sessionId = Util.uuidv4();
        this.importantQueue = [];
        this.firstSession = 0;
    }

    isActive() {
        return this.isStarted;
    }

    onChannelConnected() {
        this.channel.send(new CSInitSession(this.sessionId));
    }

    onChannelDisconnected() {
        if (!this.forceDisconnected) {
            this.channel.reconnect();
        }
    }

    onMessage(message) {
        if (message instanceof SCCloseSession) {
            this.forceDisconnected = true;
            this.channel.disconnect();
        }
        else if (this.maxSCSequence < message.scSequence) {
            this.maxSCSequence = message.scSequence;
            var csSequence = message.csSequence;
            while (this.importantQueue.length > 0) {
                var m = this.importantQueue[0];
                if (m.csSequence <= csSequence)
                    this.importantQueue.shift();
                else
                    break;
            }
            message.onData();
        }

    }

    send(message) {
        this.send(message, false);
    }

    send(message, important){
        if (this.isStarted && !this.forceDisconnected){
            message.timeClient = new Date().getTime();
            if (!(message instanceof FPing)) {
                message.csSequence = (++this.maxCSSequence);
                message.scSequence = (this.maxSCSequence);
            } 
            if (important){
                this.importantQueue.push(message);
            }
            this.channel.send(message);
        }

    }

    newSession() {
        LogUtil.info("new session");
        this.isStarted = true;
        this.maxCSSequence = 0;
        this.maxSCSequence = 0;
        this.importantQueue = [];
        this.forceDisconnected = false;
        for (listener in this.listeners) {
            if (this.firstSession == 0){
                listener.onFirstSession(this);
            }
            listener.onNewSession(this);
        }

        this.firstSession++;
    }

    start() {
        this.channel.start();
        this.sendPing();
    }

    sendPing() {
        this.send(new FPing());
        setTimeout(
            () => { this.sendPing(); }, 5000)
    }

    continueCurrentSession(){
        while(this.importantQueue.length > 0){
            var m = this.importantQueue.shift();
            this.channel.send(m);
        }
        
        for (listener in this.listeners) {
            listener.onContinueCurrentSession(this);
        }
    }
}

