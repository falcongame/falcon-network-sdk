import SCInitSession from "../message/SCInitSession"
import LogUtil from "../util/LogUtil";
export default class FChannel{
    
    start(){
        throw new Error('You have to implement the method start!');
    }

    reconnect(){
        throw new Error('You have to implement the method reconnect!');
    }

    disconnect(){
        throw new Error('You have to implement the method disconnect!');
    }

    sendMessage(message){
        throw new Error('You have to implement the method sendMessage!');
    }

    send(message) {
        LogUtil.debug(message);
        this.sendMessage(message);
    }

    onConnected(){
        this.session.onChannelConnected();
    }

    onDisconnected(){
        this.session.onChannelDisconnected();
    }

    getSession(){
        return this.session;
    }

    setSession(session){
        this.session = session;
    }

    onMessage(message){
        LogUtil.debug(message);
        if (message instanceof SCInitSession) {
            if (message.state == 0) {
                this.session.newSession();
            } else {
                this.session.continueCurrentSession();
            }
        } else {
            this.session.onMessage(message);
        }
    }

}