import SCTest from "../../../../test/js/messages/SCTest";
import FPong from "../message/FPong";
import SCInitSession from "../message/SCInitSession";

export default class FSessionManager{
    constructor() {
        if (!FSessionManager.instance) {
            FSessionManager.instance = this;
        }

        return FSessionManager.instance;
    }

    static getInstance() {
        if (!FSessionManager.instance) {
            FSessionManager.instance = new FSessionManager();
        }
        FSessionManager.instance.messages = new Set();

        FSessionManager.instance.register(new FPong());
        FSessionManager.instance.register(new SCInitSession());
        FSessionManager.instance.register(new SCTest());
        return FSessionManager.instance;
    }

    register(message){
        this.messages.add(message);
    }
}