import FMessage from "./FMessage";
export default class SCCloseSession extends FMessage{
    constructor(){
        super();
        this.event = "f_sc_close_session";
    }

}
