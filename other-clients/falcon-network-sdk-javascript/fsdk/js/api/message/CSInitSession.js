import FMessage from "./FMessage";
export default class CSInitSession extends FMessage{
    constructor(sessionId) {
        super();
        this.sessionId = sessionId;
        this.event = "f_cs_init_session";
    }

}