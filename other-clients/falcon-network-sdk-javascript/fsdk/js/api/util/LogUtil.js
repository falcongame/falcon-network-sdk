import FPing from "../message/FPing"
import FPong from "../message/FPong"

export default class LogUtil{
    static debug(message){
        if ((message instanceof FPing) || (message instanceof FPong))
        return;
        console.log(message);
    }

    static info(text){
        console.log(text);
    }
}