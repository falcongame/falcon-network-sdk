import FChannel from "../api/session/FChannel"
import io from 'socket.io-client';
import FSessionManager from "../api/session/FSessionManager";
import LogUtil from "../api/util/LogUtil";

export default class FSocketIOChannel extends FChannel {
    constructor(uri) {
        super();
        this.uri = uri.substring(0, uri.lastIndexOf("/"));
        this.context = uri.substring(uri.lastIndexOf("/") + 1);
    }

    start() {

        this.ws = io(this.uri, {
            path: `/${this.context}`,
            transports: ['websocket']
        });

        this.ws.on('connect', () => {
            LogUtil.info("connected!!!");
            this.onConnected();
        });

        this.ws.on("disconnect", () => {
            LogUtil.info("disconnect!!!");
            this.onDisconnected();
        });

        this.ws.on("error", (error) => {
            LogUtil.info(error);
            this.onDisconnected();
        });

        this.ws.on("reconnect", (attempt) => {
            
        });

        this.ws.on("reconnect_attempt", (attempt) => {
            
        });

        this.ws.on("reconnect_error", (error) => {
            
        })

        this.ws.on("reconnect_failed", () => {
            
        });

        FSessionManager.getInstance().messages.forEach(fMessage => {
            this.ws.on(fMessage.event, data => {
                data.event = fMessage.event;
                Object.assign(fMessage, data);
                this.onMessage(fMessage);
            })
        })

    }

    reconnect(){
        
    }

    disconnect(){
       this.ws.disconnect();
    }

    sendMessage(message){
        this.ws.emit(message.event, message);
    }
}