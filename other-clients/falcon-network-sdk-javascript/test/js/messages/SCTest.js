import FMessage from "../../../fsdk/js/api/message/FMessage";

export default class SCTest extends FMessage {
    constructor() {
        super();
        this.event = "s_test";
    }

    onData(fClient, sTest){
        console.log(this.text);
    }
}
