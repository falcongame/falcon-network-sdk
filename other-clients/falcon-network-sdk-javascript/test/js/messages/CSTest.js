import FMessage from "../../../fsdk/js/api/message/FMessage";
export default class CSTest extends FMessage{
    constructor(text, x){
        super();
        this.text = text;
        this.x = x;
        this.event = "c_test";
    }
}