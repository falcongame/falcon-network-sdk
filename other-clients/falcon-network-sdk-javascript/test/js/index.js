import FSessionManager from "../../fsdk/js/api/session/FSessionManager"
import FSession from "../../fsdk/js/api/session/FSession"
import FSocketIOChannel from "../../fsdk/js/socketio/FSocketIOChannel";
import CSTest from "../js//messages/CSTest"

var channel = new FSocketIOChannel("ws://localhost:11112/test");
var session = new FSession(channel);
session.start();
var i = 0;
function sendTest(){
    i++;
    session.send(new CSTest("Hello " + i, i), true);
    setTimeout(
        () =>{sendTest();}, 5000)
}

sendTest();




