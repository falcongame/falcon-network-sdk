const path = require('path');

module.exports = {
  entry: './test/js/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
};