﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Falcon
{
    public class SCTest : SCMessage
    {
        public string text;
        public int y;

        public override string GetEvent()
        {
            return "s_test";
        }

        public override void OnData()
        {
            Debug.Log(text);
        }
    }
}

