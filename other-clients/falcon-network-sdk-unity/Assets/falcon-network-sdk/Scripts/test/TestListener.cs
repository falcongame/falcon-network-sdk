﻿using System;
using System.Threading;

namespace Falcon
{
    public class TestListener : ISessionListener
    {
        public void OnChannelConnected(FSession session)
        {

        }

        public void OnChannelDisconnected(FSession session)
        {

        }

        public void OnContinueCurrentSession(FSession session)
        {

        }

        public void onFirstSession(FSession fSession)
        {
            int i = 0;
            new Thread(() =>
            {
                while (true)
                {
                    i++;
                    fSession.Send(new CSTest("Hell " + i, i), true);
                    Thread.Sleep(5000);

                }
            }).Start();
        }

        public void OnTimeout(FSession fSession)
        {

        }


        public void OnNewSession(FSession fSession)
        {

        }
    }
}
