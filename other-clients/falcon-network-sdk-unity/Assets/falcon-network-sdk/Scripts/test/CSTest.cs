﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Falcon
{
    public class CSTest : CSMessage
    {
        public string text;
        public int x;

        public override string GetEvent()
        {
            return "c_test";
        }

        public CSTest(string text, int x)
        {
            this.text = text;
            this.x = x;
        }
    }
}


