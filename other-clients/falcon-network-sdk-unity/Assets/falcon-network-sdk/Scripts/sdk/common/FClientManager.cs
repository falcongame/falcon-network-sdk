﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Falcon
{
    public class FClientManager
    {
        private FClientManager()
        {

        }
        private static FClientManager instance = null;
        public static FClientManager Instance
        {
            get {
                if (instance == null)
                {
                    instance = new FClientManager();
                    instance.Init();
                }
                return instance; 
            }

        }

        private IEnumerable<Type> scClasses;
        private void Init()
        {
            scClasses = typeof(SCMessage).Assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(SCMessage)));
        }

        public IEnumerable<Type> GetSCClasses()
        {
            return scClasses;
        }
    }
}


