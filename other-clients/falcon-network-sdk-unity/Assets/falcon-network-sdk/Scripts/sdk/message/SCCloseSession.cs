﻿using System;
namespace Falcon
{
    [Serializable]
    public class SCCloseSession : SCMessage
    {
        public int reason;

        public override string GetEvent()
        {
            return "f_sc_close_session";
        }

        public override void OnData()
        {

        }
    }
}

