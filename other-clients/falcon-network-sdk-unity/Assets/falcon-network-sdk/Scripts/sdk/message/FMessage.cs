﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Falcon
{
    [Serializable]
    public abstract class FMessage
    {
        public long csSequence;
        public long scSequence;
        public long timeServer;
        public long timeClient = DateTimeOffset.Now.ToUnixTimeMilliseconds();
        public bool needResponse;
        public abstract string GetEvent();

    }
}

