﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Falcon
{
    [Serializable]
    public abstract class SCMessage : FMessage
    {
        public abstract void OnData();
    }
}

