﻿using UnityEngine;

namespace Falcon
{
    public class LogUtil
    {
        public static void debug(FMessage message)
        {
            if (message is FPing || message is FPong)
                return;
            if (message is SCMessage)
                 Debug.LogWarning("<color=#779933>" + System.DateTime.Now + " -- " + message.GetType().ToString() + ": " + JsonUtility.ToJson(message) + "</color>");
            else
                Debug.LogWarning("<color=#0092cc>" + System.DateTime.Now + " -- " + message.GetType().ToString() + ": " + JsonUtility.ToJson(message) + "</color>");
        }
    }
}
