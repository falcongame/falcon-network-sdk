﻿using BestHTTP.SocketIO;
using PlatformSupport.Collections.ObjectModel;
using System.Collections.Generic;
using System;
using UnityEngine;
using BestHTTP.SocketIO.JsonEncoders;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace Falcon
{
    public class NetManager
    {

        private static NetManager _instance = null;

        public static NetManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new NetManager();
                    _instance.LoadEvents();
                }

                return _instance;
            }
        }

        public static void Destroy()
        {
            if (_instance != null)
            {
                _instance.manager.Close();
                _instance = null;
            }
        }

        private string sessionId;
        private IEnumerable<Type> scanTypes;  

        private SocketManager manager;
        public Action<SocketManager> OnStartListening;
        public Action OnServerDisconnected;
        public Action OnServerConnected;

        public void Start(string uri)
        {
            this.uri = uri;
            NetManager.Instance.OnStartListening = manager =>
            {
                this.manager = manager;
                manager.Socket.On(SocketIOEventTypes.Connect, OnServerConnect);
                manager.Socket.On(SocketIOEventTypes.Disconnect, OnServerDisconnect);
                manager.Socket.On(SocketIOEventTypes.Error, OnError);
                manager.Socket.On("reconnect", OnReconnect);
                manager.Socket.On("reconnecting", OnReconnecting);
                manager.Socket.On("reconnect_attempt", OnReconnectAttempt);
                manager.Socket.On("reconnect_failed", OnReconnectFailed);
            };
            ObservableDictionary<string, string> dict = new ObservableDictionary<string, string>();
            dict.Add("version", Application.version);
#if UNITY_ANDROID
            dict.Add("platform", "android");
#elif UNITY_IOS
            dict.Add("platform", "ios");
#endif
            TimeZone tz = TimeZone.CurrentTimeZone;
            int addHours = tz.GetUtcOffset(DateTime.Now).Hours;
            dict.Add("timezone", addHours.ToString());
            dict.Add("language", Application.systemLanguage.ToString());

            SocketOptions options = new SocketOptions
            {
                AutoConnect = false,
                ConnectWith = BestHTTP.SocketIO.Transports.TransportTypes.WebSocket,
                AdditionalQueryParams = dict
            };
            manager = new SocketManager(new Uri(uri), options);
            manager.Encoder = new LitJsonEncoder();
            OnStartListening(manager);
            FireEvents();
            manager.Open();
         
        }

        private void LoadEvents()
        {
            scanTypes = typeof(SCMessage).Assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(SCMessage)));
        }

        private void FireEvents()
        {

            foreach (var type in scanTypes)
            {
                if (type.IsSubclassOf(typeof(SCMessage)))
                {
                    SCMessage message = (SCMessage)Activator.CreateInstance(type);
                    manager.Socket.On(message.GetEvent(), (socket, packet, args) =>
                    {
                        JArray array = JArray.Parse(packet.Payload);
                        var json = array[1].ToString();

                        message = (SCMessage)JsonConvert.DeserializeObject(json, type);

                        LogUtil.debug(message);

                        message.OnData();
                    });
                }
            }
        }


        public void Send(CSMessage message)
        {
            LogUtil.debug(message);
            manager.Socket.Emit(message.GetEvent(), message);
        }


        private string uri = "";


        public void Restart()
        {
            this.manager.Close();
            Start(uri);
        }

        void OnServerConnect(Socket socket, Packet packet, params object[] args)
        {
            Debug.Log("Connected");
            if (sessionId == null || sessionId.Length == 0)
            {
                sessionId = System.Guid.NewGuid().ToString();
            }
            Send(new CSInitSession(sessionId));

            OnServerConnected?.Invoke();
        }

        void OnServerDisconnect(Socket socket, Packet packet, params object[] args)
        {
            Debug.Log("OnDisconnect");
            OnServerDisconnected();
        }

        void OnError(Socket socket, Packet packet, params object[] args)
        {
            Debug.Log("OnError");
        }

        void OnReconnect(Socket socket, Packet packet, params object[] args)
        {
            Debug.Log("Reconnected");
        }

        void OnReconnecting(Socket socket, Packet packet, params object[] args)
        {
            isSessionStarted = false;
            Debug.Log("Reconnecting");
        }

        void OnReconnectAttempt(Socket socket, Packet packet, params object[] args)
        {
            Debug.Log("ReconnectAttempt");
        }

        void OnReconnectFailed(Socket socket, Packet packet, params object[] args)
        {
            Debug.Log("ReconnectFailed");
        }


        public bool isSessionStarted = false;

        public void StartSession()
        {
            isSessionStarted = true;

        }

        public bool IsOnline()
        {
            return isSessionStarted && manager != null && manager.Socket != null && manager.Socket.IsOpen;
        }
    }
}