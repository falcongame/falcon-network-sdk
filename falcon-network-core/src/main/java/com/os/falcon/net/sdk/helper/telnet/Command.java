/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.net.sdk.helper.telnet;

import java.util.ArrayList;

/**
 * 
 * @author quanph (quanph@onesoft.com.vn)
 */
public abstract class Command {

    protected ArrayList<String> argList = new ArrayList<String>();

    public Command() {

    }

    void setArgs(ArrayList<String> argList) {
        this.argList = argList;
    }

    public abstract String execute();
}
