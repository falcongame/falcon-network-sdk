package com.os.falcon.net.sdk.helper.telnet;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * 
 * @author quanph (quanph@onesoft.com.vn)
 */
public class TelnetServerHandler extends SimpleChannelInboundHandler<String> {

    @Override
    protected void channelRead0(ChannelHandlerContext chc, String request) throws Exception {
        request = request.trim();
        CommandManager.getInstance().runCommands(chc.channel(), request);
    }
}
