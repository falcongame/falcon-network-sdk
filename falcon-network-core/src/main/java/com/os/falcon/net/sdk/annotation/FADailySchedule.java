/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.net.sdk.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @author quanph (quanph@onesoft.com.vn)
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(FADailySchedules.class)
public @interface FADailySchedule {
  String time() default "00:00:00";
  String startDate() default "31/03/2020";
  String endDate() default "31/03/2050";
  String dayOfWeek() default "1,2,3,4,5,6,7"; //1=CN,2=t2,...,7=t7
}
