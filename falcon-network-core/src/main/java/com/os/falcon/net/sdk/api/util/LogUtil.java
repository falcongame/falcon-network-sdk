/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.net.sdk.api.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.os.falcon.network.api.message.FMessage;
import com.os.falcon.network.api.message.FPing;
import com.os.falcon.network.api.message.FPong;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author phamquan
 */
public class LogUtil {

    static int numError = 0;
    public final static int MAX_ERROR = 10;
    static Logger logger = Logger.getLogger(LogUtil.class);

    public static void info(String message) {
        logger.info(message);
    }

    public static void debug(String message) {
        logger.debug(message);
    }

    public static void error(String message, Throwable t) {
        numError++;
        if (numError < MAX_ERROR) {
            logger.error(message, t);
        }
    }

    public static void error(Throwable t) {
        logger.error(t.getMessage(), t);
    }

    public static void init(String configFilePath) {
        PropertyConfigurator.configure(configFilePath);
    }

    public static void debug(FMessage message) {
        if ((message instanceof FPing) || (message instanceof FPong)){
            return;
        }
        try {
            LogUtil.debug(message.getClass().getSimpleName() + ": " + FMessage.objectMapper.writeValueAsString(message));
        } catch (JsonProcessingException ex) {
            ex.printStackTrace();
        }

    }

}
