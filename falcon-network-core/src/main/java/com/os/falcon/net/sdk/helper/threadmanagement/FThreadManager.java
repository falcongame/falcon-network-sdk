/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.net.sdk.helper.threadmanagement;

import com.google.common.collect.MapMaker;
import java.util.Map;
import java.util.Set;
import org.reflections.Reflections;
import com.os.falcon.net.sdk.annotation.FAAutorun;
import com.os.falcon.net.sdk.api.util.LogUtil;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import com.os.falcon.net.sdk.annotation.FAThread;

/**
 * 
 * @author quanph (quanph@onesoft.com.vn)
 */
public class FThreadManager {

    private static FThreadManager _instance = null;
    Map<String, FThread> fThreadInstances = new MapMaker().makeMap();
    Map<String, Class<?>> threadClasses = new MapMaker().makeMap();

    ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public static FThreadManager getInstance() {
        if (_instance == null) {
            _instance = new FThreadManager();
        }
        return _instance;
    }

    public void start() throws Exception {
        // load event classes
        Reflections reflections = new Reflections("");
        Set<Class<?>> threadClass = reflections.getTypesAnnotatedWith(FAThread.class);

        for (Class<?> item : threadClass) {
            threadClasses.put(item.getName(), item);
            boolean autorun = item.isAnnotationPresent(FAAutorun.class);
            if (autorun) {
                startThread((FThread) item.newInstance());
            }
        }

    }

    void startThread(FThread fThread) {
        fThreadInstances.put(fThread.getName(), fThread);

        int delayTime = fThread.getNextRuntime();
        if (delayTime == 0) {
            fThread.setState(FThread.STATE_RUNNING);
            fThread.start();
        } else if (delayTime > 0){
            fThread.setState(FThread.STATE_IN_SCHEDULE);
            LogUtil.info(fThread.getName() + " will be started after " + delayTime + "s");
            scheduler.schedule(new Runnable() {
                @Override
                public void run() {
                    fThread.start();
                }
            }, delayTime, TimeUnit.SECONDS);
        }
    }

    void onThreadStop(FThread fThread) {
        fThread.setState(FThread.STATE_STOPPED);
        fThreadInstances.remove(fThread.getName());
        int nextTime = fThread.getNextRuntime();
        if (nextTime > 0){
            startThread(fThread);
        }
        
    }

    void deleteThread(FThread fThread) {

    }

    public void stopThread(FThread fThread) {

    }

    
    public void print(){
        for (FThread fThread: fThreadInstances.values()){
            LogUtil.info(fThread.getName() + " " + fThread.getState());
        }
    }
}
