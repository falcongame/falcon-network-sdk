package com.os.falcon.net.sdk.api.util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import javax.crypto.Cipher;

/**
 *
 * @author quangpk
 */
public final class EncryptDecrypt {

    //https://www.novixys.com/blog/how-to-generate-rsa-keys-java/
    //https://www.roytuts.com/encryption-and-decryption-using-rsa-in-java/
    private static final String ALGORITHM = "RSA";
    private static final int ALGORITHM_BITS = 8192;
    private static final Charset CHARSETS = StandardCharsets.UTF_8;

    private static void GenerateKeys() {
        KeyPairGenerator generator = null;
        try {
            generator = KeyPairGenerator.getInstance(ALGORITHM);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (generator != null) {
            generator.initialize(ALGORITHM_BITS);
            KeyPair keyPair = generator.genKeyPair();
            Key pvt = keyPair.getPrivate();
            Key pub = keyPair.getPublic();

            Base64.Encoder encoder = Base64.getEncoder();

            System.out.println("-----BEGIN PRIVATE KEY-----");
            System.out.println(encoder.encodeToString(pvt.getEncoded()));
            System.out.println("-----END PRIVATE KEY-----");
            System.out.println();
            System.out.println("-----BEGIN PUBLIC KEY-----");
            System.out.println(encoder.encodeToString(pub.getEncoded()));
            System.out.println("-----END PUBLIC KEY-----");
        }
    }

    private final static String PRIVATE_KEY_BASE64_STRING = "MIISQwIBADANBgkqhkiG9w0BAQEFAASCEi0wghIpAgEAAoIEAQDKxttg+h2VRo38THcfDP80DuWtdYB3hsQN39u5b/jPTmbHgiIo88LFMPICKGQSnV4QCCFRD9HNX0efa5G2SrBqD24LZ9CNV0ZskAhZVW6iRDD963+BvxC3HCdEcIKZ/uHPc+ozhsfB2BzMgVZDlQ0HFY5AVV8FvqVJd7MeIFCN7KB+EBwJo+rsGKgUoTCq6cEyDQJmIA+ST8yb/BQDuRpKYmOQj9fz+AAR0qn89eBTHSZ/bZUGLXXj2yuayAxHUqi3pY9AfCtNTsjo2+Q7Lb8d4EMBXBFp3GDlgksc984g1tAgCzCDtXF1AST1FYE72DlxCJNd4ZXLUFYxUzu1xwcmYZkXlDT6cf7a4tsf85Dj/lBScXg2UlQ3/GB9C7Q1mER1Dznvl61h76SW80vFw8HUySJlSny0dZXAiQoYCnaAoGqtWlP7AXHAWRGqIxRgcF955f/h/ut7vrDvqB5593UjJgSAV8DLkqw+3OL8fIOjoL3y6EOfMexDgEydrmXPcJ8pTlqZlu+nCMMuSMBngOLeFy6R+DezKhXDIc+tKIbsubR02MUTk3L3krTnojqorVtJ0n/ZX/esDuYOJk+PCQqdgtPNlA8ce4+vNhXo+4QIgejFNKZL4wuejAhfY0QXsbfndNQHaT8RU/7a6K1jbuefP/32O/p6XvuHdHIEnMe5KAUQ7ws042ysgzHZ+akhetQ0i8hTXythpMUnj6WXOHQOws1vyqR0JcY1AFscmVpI0jopn6mx4EKzlP3d6V6vGdoEVgEP0CaAESPbAtdqtHceI+JswBP7JNwVczFLKrNt/A62JJvjBdLEi+I9NpCSSsBkBejJNAk9O9Q45/G8nCfgaIvYpaSv3PqP14KRfndBNdL5E8YCnuyazdZ9fA7Ed5PcQHa9sG40nHvUrNYXKqhASygAshHnT2/gAieYZIRb7YHKPaWuldNTgi74OAFieZGOf7FMewFpUZKhz44IoQtg6YH0T7OcI7sbibTPivnjgMjzII1SNqRjf0Zli9hC5qNMCUxEEQe6+HEG/yQIQYGiemXfpGDdM9xvdeBh8JYHJxHMnXMdQksgTG+rJtqYhGGu84+Nx5mitCFfio5whk9X4ooHgoU5R4GbtsmthDy4AVbwNZxDdxzUW/CjlqDeP2FKthjEEbZvwIlES9YfgKdXf16gz42e7dfHtLdj9yOqDk2nVP2KP6Q3TT8CuRVQ40Oadqkysl73xFqFzHEP7zLrkOmq/LOJ9e35kJB+nkhgSYio/FOfJjZheieAxGOUwEdPwmHCeOpiDvW6vrOOrhf8DjqV2cre7+rmlSqXG09AL62mDDGRVdg9FxfOo2nhi5LZbr2q10s/TPwho58f6ba/AgMBAAECggQAMuEKkp6vZZE6dx/Ik+Xy8uD6CDf/b/5EmyacPz81Mu79IvooEBZ5vHi3gIsZ8QHDZEpl8a0Ce4F4uyuGeDLtaO9OMVZvIcfQR9UsAx5IkDaEBHJen/ONAiTyRV3lpzYo+qCfFyauYJkVQMsDFhoEFs71znadTRe06WY7b0e0bqf87SSlpFWY0HyvT93SFFBqxWA2ReGthoy3TjxF2VlMm3Vsfg1ZpkGH4vD07h/TlYcu7bJVPiOR/QY1PrULP6oh41KLSybcxp0KFEN4VND5r0liBqlPRTyio3O53R7gfaTou+GS3rlcKnuLBmPK9XUflJkOQPQlXcpelinIRagWxy6d9yic0mpgBBP/31iB9nMNPGJ0Q00NDK8qlrWlFf2Kz8RuSQvJ2VUD+0w4qvqfXADLvJagC7elR5YbDkUxzrxxh+WuRMDsBdNTd1kt97S6gB+RwObxty71vmx0mS3aGmOjpYBfQ7Uduvg5jBjB/wCKKLTWos8n9RfwIXBs40vi475hRUB2CXdNYgOnI1FXm1o9uSqK702UAE4vicctc6B1AwyRBoRsyGnrMmOmTESTL0a+BZDq2CU03EAInl8qZrM46dBIqmDmgJQVk0voqSnPegvn3gquE64XO7fwkfFpr43E1cc2p+ZngA3504U0YOTxmCMcii/i9O3m3flos7BAjuzZE8Io9hTcMgyJQvrvMhLDL+cg2rZhoQrz5/aHW1LXoCjoUfR+/b48S4maA0VibQCfQeGlKcujUpgUCVkJ4YFt+FmnVAwCIZOjRBLL0M60TEkp2SXNYjIvOhBvFvQmavutRqxO1dHKxQuWZCS9EuS1R0vjf9zghVr8Li42g1s4Rkyvj//xQbnM36Q9fLQzO0aF4CfANECVdMQFUAsWohD40B4dmlJv9HMnXT2E3/wbbszU3+KbrhfhClZ6JhdUHSeC+sob+9F+6B7l6gTrBfNT3ShfIuv6W7sX0JalT6AX/ge1DFjGeZxr7wiEGZevnYVOmAFBAqwl/avLj226FUBxyzf0eaL/bw6nnmrq/ID2tWS38YJ28LGWWsVtLwnYBalqDKUw15NW/NJsA+GK1pXam5cPIuw4TZUNvRtDyDt6NjRyFuqm7daFHfqoRVS5dMSxeQT/Ol6ebvW1NvwthkfSyNGjDYvlDelvC9N+BoU+920pHVEeW7d/zhKEekMNOylLZaVDTz3hTE/Wx495i1/bu6/pFLfe1e3mSDhGKWMuLsG+7ROe84B7EpjFIsFpMjWVmSwiFvi18seNkXXzQelzmo5CFFw6aZzPnO+1la5lg/xY0rHCL3V3yYEkQrP/ux9f7KwlaUatFCITu34O+LgCTXaY1Pjnc2GauTnlgQKCAgEA636DwSQtmT3twRNZfjMY4ylHhqN1+8pDJKog55ZcyfrUzbcLAMmXrW+DApDDNdopwoRzm9x+b+hjpMTeapnLnMuS1fPHiKxvrsmnEUcaX4HMFDMClQcoUqEX+b4atjGmQtXP3xKnA8MX59kG2VLk852cYnbddrnKQE7xAwMCkLbSr/9o3r/u/fdiZPyjuVDUYRBwUTteQshkz4rH/37kNTOBFCEd0Av3yyg2rSXdDvtBFdUiWd0HDzuUnP0RMYsPt1eCqgOU6npdBM9iIlYo3NOXEfXbzGjQaD5oWNWHeWZSWHdWGWrEKAZSXNTQuIbCwUvsYjPUGi9nbbXMvlq1mjpYh7yQzg5uxX22YcZi3oZ8SZdA4Ay64onK5OFa1PrnL4e5qh4h8/NDzJ9cHaSwA2oaAdRQAjH9MOXfCDLByUxjFkbOl7kQYrRYd/diYrwHTXX57569BasrzI/Nzv/JoCvEatKynwV9WESjL9hSmYWdLQFdiyDXZPr7IaRsvqMt9VfAWXa7j2LyTuHjDn5JC7wVYcUGdwV7z3fJKM+25yCtbQ1BP1NHu10RUZzfTGLtGDKB33WnrY53e+nzor/UzxMOGAQQCvQoeUV6PGLS8U6950ogkfueg66VVggsEY8w2l7mhuxjmdtMbsL1Su2DQmUlYn3j5Ur1PrCgqaKsv+8CggIBANxvBtg5AxgdOmz+iNOSN5ASJfkJXBDc05RLK4k2r3Y9HpZfgSxPAKmUxhwtVyVKXOeyk0wBFT5oiqDsxtlom9eN5hxyqOjWgWplBkLvwnI4+uDsQEvKtKK8qSjJ6E3TO18pt48hzCJ6r8/wop3IWOQCGm/HdBuj4alM/BPCN+/wFNZIYLzFx3uqp9OkZ7VEu52hvuZx+hqIuvNPtvQ+qEBghK4hal0D9up6uFugNhbZSyd780v8tyqEXV08/bEmJ+whOOUW91a6bAzN4+m+njfkeEFsuYsilMLll1nh+8ZQcQ69jkKDW2uwmhU7e79uHphKxyDiuxMkAko+ApW3OcrQJBw2pQhkX27Z9MZJzQ36HhF5GCIgVHpQpJVO9SfKEdE22zI0QZISwIxgZchABrQzxsqKrMqoxPZs4hXOiMp9V69CL+gX1gKP+nOEcVZ3Umt4ZM0fbS5GDbEpNCyGsnYrYKGqUAYNzhRjQVNP67SddkDedmwxFbTLg0qPveqGmnvVbQ7zj2Zl3cNSE1D/dyRVTC7GmfWy4C6tauZDmuXT1bs7SAmvQvcH3NzRXvcT3Sf78jSp/yM12s1JQ3Po+pcwPSNiV7zzv8WpRtARhovo2JbC8gBsFtqW+C6+fu/qh9kVOA04V8bcJ5h/9H2FGSbQN0AQen4faGwcHlNp3KYxAoICAQDPApW7UJws2z2GvgWrZCt6dK0Fc+ehAPno3wyxx4Glo7MxarhnZs7IjP/zX2Hakv6mmVwvJLKYmQYOr2h8+Nj96g4XuYYrZLmAQo90JrbOQgEtuHGKo5IUoLu8tBozEo3+rolepLzgF72J90OIAtB0PpZcD/U6CrP5t+sxJJI6uBuYzsJXHQ1vfumlsFgT7TPkZ1j0b2W6+wFzV6HbvKXLBo1vLnVQj1Sx55W4u1IJe2CJWqQprpwoDc3j+qoEjSOxnWGlJiTfwdTHB65SVL7y76UbCq6+j1y0Zje/72R5mTLFxZGQIkRwkq4QIJuhugS1epI++SR4KDeZP3jN1FtTSfrMxzvbhuBlBs8uQABlcH8NCcLjsQ0PWuVaMadxFWXoaD0TiA18rtlopKWLAVwghEJ8XTA/3cWeGFPolwW2eEmaKYAt62a1/abPPk3kUTlyJEUjGL08oG2OEtNuJXgOLqso5DJUi7TVxPBWfKiPf97c0xhx1trBT8c2mZ1mL3eU9iCGBFN4BhjOss6vzN9VS27YVSQLuGxPwhK4p2822YjaFg7HJeQmlfdFcMryEPwuiyeTUY+mh1gNO2kKJetiZHO9NMUNe/egMyzyrESWDwvsp0LeUPvU8cEgsWpbvS3KqCWpcV9Uvmv/6a1KwmLiGJu2asfMpgnTo9x3giKtTQKCAgBxEvr923D847Fey8rATNb3rneQIf76H2zhOxYgEs865etU07TdV8FNPCXLum5jkJvLtszcRgnxs4q5Nfxi9chLcFzD5wVCOxazzxLBL2AdW6FF06q/tkGm4QQdCkWB9oMUM8AXSgz++tRI5HVmXqlKfNG22Sd0LkLJp9X/BLfQ+uSVbxdAfmvvwgDJDiKs148uMxRL/dd/2PedbBy7eZCxRVGKSVXYVPpKGZ2MUsisO9bTcD/DkOUciD2kYG76ekosVHZs/bTu5j4a61qoQsHChg4x8WXUO2738vIKh2OxvP3hw9a/w8YXncXMcnDuJ7zF/kXbVrMEBIqwLtMAnballNORLW7GuUZDBMcNGDp5mv3llKqOhusOu2NRu+kB+iPdoDgcrrWcCsZ+u8w6Rq2pro2gxDYS1zQB/KSU7xc4V8es/VS5x9BeDCE7NIs3AYpj2qRKnH5TKFhJsosy8/2DpIizJTxbfqVhy/BsniiYw9UvPW6d3Sf3UBYPH2EaduWz9C03aDT9H+/8C5QhDoq9NowXyoSbhqB9ETQu3isNDfcrTiOL5D2DnqLrGvON5sChjBoYbcOU+kE84QIa6T3yZCpYhk/fe2G6MTVD8lmEV3MTEvk0wj2O77ZrtAhFZZc+AiZ4kZhlR/V/wtNVt/rI8DIHSOys2cuk+V+NBlbLYQKCAgEA07CZsj9vK/qfyvTaolezIUykQU5aLBjR6BSRzqXok6ekxfCOgVMxPTTdwU5MWfdVIMF1UE49rxqbgcA82lwaG3YezSwiwVLsfowrswBuRWQ8tRNWTd/Ifu4JO2KZ9+tdSRJfmKnF46ISW8OOhRgv2JUEumSbA8Sbmy9sMgQK7JUrgMHsro0qVDwb2xMIkA/8wIu9uSSoVa2UKdn+fxweUJwc27rY4z/aL1EXYzV1UzRUiQqJTBvjl+uILwNtGGnS7MJ8xB4DABRw/mBC9DIHn0n+LIIMICKwRghDAhiixysYzFVCZPb78L7258neWdXRKBb4yC+GKqSdDWtQ1DKyp7qPgNzaqAqMWaXCGoMo/srUsepY3rypjCI/4mxcxEo6ShyUlkJ/VtsyZMjNL00LBQ97fWGWf/T0wOoVgMNYtT3YWDOSZUIca/dNTVzolKjSf2Ci92njT/iCX6MnNp5LI791cBF051Rx9lY5PVNNCAA0zBT7rVdq6eZ5aDCBQQ4S18rKmLrydWxN1KVpVBTIq3pwLx/Nx26TASP6jHvVWC4s+KSViFy1gXRwJMeWW2nGgld+Pb4PoU1B0aaaytyO2KP+kNvXrRO7sdZDQcY1Eagb/slR2HLVf7nzN6zcvTc33ZYQ98xUT7Frrqm1H+EaRoBPOl8q+nEdAG5hXh+pRW8=";
    private final static String PUBLIC_KEY_BASE64_STRING = "MIIEIjANBgkqhkiG9w0BAQEFAAOCBA8AMIIECgKCBAEAysbbYPodlUaN/Ex3Hwz/NA7lrXWAd4bEDd/buW/4z05mx4IiKPPCxTDyAihkEp1eEAghUQ/RzV9Hn2uRtkqwag9uC2fQjVdGbJAIWVVuokQw/et/gb8QtxwnRHCCmf7hz3PqM4bHwdgczIFWQ5UNBxWOQFVfBb6lSXezHiBQjeygfhAcCaPq7BioFKEwqunBMg0CZiAPkk/Mm/wUA7kaSmJjkI/X8/gAEdKp/PXgUx0mf22VBi1149srmsgMR1Kot6WPQHwrTU7I6NvkOy2/HeBDAVwRadxg5YJLHPfOINbQIAswg7VxdQEk9RWBO9g5cQiTXeGVy1BWMVM7tccHJmGZF5Q0+nH+2uLbH/OQ4/5QUnF4NlJUN/xgfQu0NZhEdQ8575etYe+klvNLxcPB1MkiZUp8tHWVwIkKGAp2gKBqrVpT+wFxwFkRqiMUYHBfeeX/4f7re76w76geefd1IyYEgFfAy5KsPtzi/HyDo6C98uhDnzHsQ4BMna5lz3CfKU5amZbvpwjDLkjAZ4Di3hcukfg3syoVwyHPrSiG7Lm0dNjFE5Ny95K056I6qK1bSdJ/2V/3rA7mDiZPjwkKnYLTzZQPHHuPrzYV6PuECIHoxTSmS+MLnowIX2NEF7G353TUB2k/EVP+2uitY27nnz/99jv6el77h3RyBJzHuSgFEO8LNONsrIMx2fmpIXrUNIvIU18rYaTFJ4+llzh0DsLNb8qkdCXGNQBbHJlaSNI6KZ+pseBCs5T93elerxnaBFYBD9AmgBEj2wLXarR3HiPibMAT+yTcFXMxSyqzbfwOtiSb4wXSxIviPTaQkkrAZAXoyTQJPTvUOOfxvJwn4GiL2KWkr9z6j9eCkX53QTXS+RPGAp7sms3WfXwOxHeT3EB2vbBuNJx71KzWFyqoQEsoALIR509v4AInmGSEW+2Byj2lrpXTU4Iu+DgBYnmRjn+xTHsBaVGSoc+OCKELYOmB9E+znCO7G4m0z4r544DI8yCNUjakY39GZYvYQuajTAlMRBEHuvhxBv8kCEGBonpl36Rg3TPcb3XgYfCWBycRzJ1zHUJLIExvqybamIRhrvOPjceZorQhX4qOcIZPV+KKB4KFOUeBm7bJrYQ8uAFW8DWcQ3cc1Fvwo5ag3j9hSrYYxBG2b8CJREvWH4CnV39eoM+Nnu3Xx7S3Y/cjqg5Np1T9ij+kN00/ArkVUONDmnapMrJe98RahcxxD+8y65DpqvyzifXt+ZCQfp5IYEmIqPxTnyY2YXongMRjlMBHT8JhwnjqYg71ur6zjq4X/A46ldnK3u/q5pUqlxtPQC+tpgwxkVXYPRcXzqNp4YuS2W69qtdLP0z8IaOfH+m2vwIDAQAB";
    private static Key privateKey;
    private static Key publicKey;

    public static byte[] encrypt(byte[] original) {
        if (privateKey == null) {
            byte[] bytes = Base64.getDecoder().decode(PRIVATE_KEY_BASE64_STRING);
            PKCS8EncodedKeySpec ks = new PKCS8EncodedKeySpec(bytes);
            try {
                KeyFactory kf = KeyFactory.getInstance(ALGORITHM);
                privateKey = kf.generatePrivate(ks);
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
        }
        if (original != null) {
            byte[] encData = convert(original, privateKey, Cipher.ENCRYPT_MODE);
            return encData;
        }
        return null;
    }

    public static String encrypt(String original) {
        if (original != null) {
            return Base64.getEncoder().encodeToString(encrypt(original.getBytes(CHARSETS)));
        }
        return null;
    }

    public static byte[] decrypt(byte[] encrypted) {
        if (publicKey == null) {
            byte[] bytes = Base64.getDecoder().decode(PUBLIC_KEY_BASE64_STRING);
            X509EncodedKeySpec ks = new X509EncodedKeySpec(bytes);
            try {
                KeyFactory kf = KeyFactory.getInstance(ALGORITHM);
                publicKey = kf.generatePublic(ks);
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
        }
        if (encrypted != null) {
            byte[] decData = convert(encrypted, publicKey, Cipher.DECRYPT_MODE);
            return decData;
        }
        return null;
    }

    public static String decrypt(String encrypted) {
        if (encrypted != null) {
            return new String(decrypt(Base64.getDecoder().decode(encrypted)), CHARSETS);
        }
        return null;
    }

    private static byte[] convert(byte[] data, Key key, int mode) {
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(mode, key);
            byte[] newData = cipher.doFinal(data);
            return newData;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
