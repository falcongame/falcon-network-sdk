/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.network.api.message;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Serializable;

/**
 *
 * @author quanph
 */
public abstract class FMessage implements Serializable {
    private String messageId;
    private long csSequence;
    private long scSequence;
    private long timeServer;
    private long timeClient;
    private int numberClone = -1;
    private boolean needResponse;
    private int response;//=0 không cần response, =1 response trước khi thực thi, =2 response sau khi thực thi
    public abstract String getEvent();

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    
    
    public long getCsSequence() {
        return csSequence;
    }

    public void setCsSequence(long csSequence) {
        this.csSequence = csSequence;
    }

    public long getScSequence() {
        return scSequence;
    }

    public void setScSequence(long scSequence) {
        this.scSequence = scSequence;
    }

    public long getTimeServer() {
        return timeServer;
    }

    public void setTimeServer(long timeServer) {
        this.timeServer = timeServer;
    }

    public long getTimeClient() {
        return timeClient;
    }

    public void setTimeClient(long timeClient) {
        this.timeClient = timeClient;
    }

    public boolean isNeedResponse() {
        return needResponse;
    }

    public void setNeedResponse(boolean needResponse) {
        this.needResponse = needResponse;
    }

    public int getResponse() {
        return response;
    }

    public void setResponse(int response) {
        this.response = response;
    }
    
    
    

    public static final ObjectMapper objectMapper = new ObjectMapper();
    static {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }
    
    private String json;

    @Override
    public FMessage clone() {
        numberClone++;
        if (numberClone == 0) {
            try {
                json = objectMapper.writeValueAsString(this);
            } catch (Exception ignore) {            }
            return this;
        } else {
            try {
                return objectMapper.readValue(json, this.getClass());
            } catch (Exception ex) {
                return null;
            }
        }
    }

}
