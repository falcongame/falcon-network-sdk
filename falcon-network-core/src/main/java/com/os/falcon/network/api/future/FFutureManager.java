/*
 * The MIT License
 *
 * Copyright 2021 quanph.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.os.falcon.network.api.future;

import com.os.falcon.network.api.message.CSSCResponse;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author quanph
 */
public class FFutureManager {

    private static final FFutureManager _instance = new FFutureManager();

    static {
        _instance.start();
    }

    public static FFutureManager getInstance() {
        return _instance;
    }

    private Map<String, FFuture> messageId2Future = new ConcurrentHashMap();
    private Map<Integer, Set<FFuture>> timeout2Futures = new ConcurrentHashMap();

    private void start() {
        new Thread(() -> {
            while (true) {
                for (Integer timeout : timeout2Futures.keySet()){
                    if (timeout < (int)(System.currentTimeMillis() / 1000)){
                        for (FFuture future : timeout2Futures.get(timeout)){
                            future.onTimeout();
                        }
                        timeout2Futures.remove(timeout);
                    }
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(FFutureManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
    }

    public void put(String messageId, FFuture future) {
        messageId2Future.put(messageId, future);
        int sec = future.getTimeOut();
        if (!timeout2Futures.containsKey(sec)) {
            timeout2Futures.put(sec, new HashSet<>());
        }
        timeout2Futures.get(sec).add(future);
    }

    public FFuture get(String messageId) {
        if (messageId2Future.containsKey(messageId)) {
            return messageId2Future.get(messageId);
        } else {
            return null;
        }
    }

    public void remove(String messageId) {
        if (messageId2Future.containsKey(messageId)) {
            messageId2Future.remove(messageId);
        }
    }

    public boolean contains(String messageId) {
        if (messageId2Future.containsKey(messageId)) {
            return true;
        } else {
            return false;
        }
    }

    public void onResponse(CSSCResponse message) {
        String messageId = message.getMessageId();
        if (messageId2Future.containsKey(messageId)){
            FFuture future = messageId2Future.get(messageId);
            future.onResponse(message);
            messageId2Future.remove(messageId);
            timeout2Futures.get(future.getTimeOut()).remove(future);
        }
    }
}
