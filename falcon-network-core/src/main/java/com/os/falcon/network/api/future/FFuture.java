/*
 * The MIT License
 *
 * Copyright 2021 quanph.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.os.falcon.network.api.future;

import com.os.falcon.network.api.message.CSSCResponse;

/**
 *
 * @author quanph
 */
public class FFuture {

    public static FFuture RECEIVED = new FFuture();
    public static FFuture PROCESSED = new FFuture();

    IResponseListener responseListener;
    private int timeOut;
    private long timeSend;
    private String messageId;

    public FFuture() {
    }

    public FFuture(String messageId, int timeOut) {
        this.messageId = messageId;
        this.timeOut = timeOut +(int) (System.currentTimeMillis() / 1000);
        this.timeSend = System.currentTimeMillis();
    }

    public int getTimeOut() {
        return timeOut;
    }

    public long getTimeSend() {
        return timeSend;
    }

    public String getMessageId() {
        return messageId;
    }
    
    public void addResponseListener(IResponseListener responseListener) {
        this.responseListener = responseListener;
    }
    
    public void onResponse(CSSCResponse response){
        responseListener.onResponse(response);
    }
    
    public void onTimeout(){
        responseListener.onTimeout();;
    }
}
