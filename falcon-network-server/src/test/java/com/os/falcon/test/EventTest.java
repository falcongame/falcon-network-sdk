package com.os.falcon.test;


import com.os.falcon.net.sdk.api.util.LogUtil;
import com.os.falcon.network.server.api.FSession;
import com.os.falcon.network.api.message.FAEvent;
import com.os.falcon.network.api.message.FSMessageEvent;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author quanph
 */
@FAEvent(priority = 1)
public class EventTest extends FSMessageEvent<CSTest>{
    
    @Override
    public void onMessage(FSession client, CSTest message) {
        LogUtil.info("received");
        client.send(new SCTest("Respond: " + message.getText(), message.getX()));
    }
    
}
