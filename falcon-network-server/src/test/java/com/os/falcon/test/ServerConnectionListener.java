package com.os.falcon.test;

import com.os.falcon.net.sdk.api.util.LogUtil;
import com.os.falcon.network.server.api.FChannel;
import com.os.falcon.network.server.api.FServerConnectionListener;
import com.os.falcon.network.server.api.FSession;

/*
 * The MIT License
 *
 * Copyright 2020 quanph.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/**
 *
 * @author quanph
 */
public class ServerConnectionListener extends FServerConnectionListener {

    @Override
    public void onChannelConnect(FChannel client) {
        LogUtil.info("client connected: " + client);
        LogUtil.info("session: " + client.getSession());
    }

    @Override
    public void onChannelDisconnect(FChannel client) {
        LogUtil.info("client disconnected: " + client);
        LogUtil.info("session: " + client.getSession());
    }

    @Override
    public void onNewSession(FSession session) {
        LogUtil.info("session starts: " + session.getSessionId());
    }

    @Override
    public void onContinueSession(FSession session) {
        LogUtil.info("session continues: " + session.getSessionId());
    }

    @Override
    public void onSessionTimeout(FSession session) {
        LogUtil.info("session timeout: " + session.getSessionId());
    }

}
