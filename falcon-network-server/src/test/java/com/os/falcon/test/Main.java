package com.os.falcon.test;

import com.os.falcon.api.telnet.TelnetManager;
import com.os.falcon.net.sdk.api.util.LogUtil;
import com.os.falcon.network.server.api.FServer;
import com.os.falcon.network.server.api.FServerManager;
import com.os.falcon.network.server.socket.netty.FServerNetty;
import com.os.falcon.network.server.socket.nettysocketio.FServerNettyIO;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author quanph
 */
public class Main {
    static{
        LogUtil.init("conf/falcon_log4j.properties");
    }
    public static void main(String[] args) throws Exception {

        
        LogUtil.init("conf/falcon_log4j.properties");
        TelnetManager.getInstance().start(12222);
        FServer server1 = FServerManager.getInstance().addServer(new FServerNettyIO(11112, "/test"));//.setSSL(System.getProperty("user.dir") + File.separator + "conf" + File.separator + "keystore.jks", "123456"));
        FServer server2 = FServerManager.getInstance().addServer(new FServerNetty(11111));
        server1.addConnectionListener(new ServerConnectionListener());
        server2.addConnectionListener(new ServerConnectionListener());
        FServerManager.getInstance().start();
    }
}
