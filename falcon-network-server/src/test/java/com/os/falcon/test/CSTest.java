package com.os.falcon.test;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.os.falcon.network.api.message.FMessage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author quanph
 */
public class CSTest extends FMessage{

    private String text;
    private int x;

    public CSTest() {
    }

    
    
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    @Override
    public String getEvent() {
        return "c_test";
    }
    
}
