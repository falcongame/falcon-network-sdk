/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.api.telnet.cmd;

import com.os.falcon.api.classloader.OSClassLoaderManager;
import com.os.falcon.api.telnet.Command;
import com.os.falcon.net.sdk.api.util.LogUtil;
import io.netty.channel.Channel;

/**
 *
 * @author phamquan
 */
public class CMDloadClass extends Command {

    @Override
    public String execute(Channel channel) {
        try {
            OSClassLoaderManager.getInstance().doLoadClass();
            return "Load OK";
        } catch (Exception ex) {
            LogUtil.error(ex);
            return "ERROR";
        }
    }
}
