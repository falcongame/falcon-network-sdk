/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.api.classloader;

import com.corundumstudio.socketio.listener.DataListener;

/**
 *
 * @author phamquan
 */
public class EventListenerItem {
    private Class csClass;
    private DataListener dataListener;

    public EventListenerItem(Class csClass, DataListener dataListener) {
        this.csClass = csClass;
        this.dataListener = dataListener;
    }

    public Class getCsClass() {
        return csClass;
    }

    public DataListener getDataListener() {
        return dataListener;
    }

    public void setCsClass(Class csClass) {
        this.csClass = csClass;
    }

    public void setDataListener(DataListener dataListener) {
        this.dataListener = dataListener;
    }
    
    
}
