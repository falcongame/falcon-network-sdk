/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.api.classloader;

/**
 *
 * @author phamquan
 */
public class OSClassLoader extends ClassLoader {

    public OSClassLoader(ClassLoader parent) {
        super(parent);
    }

    public Class createClass(String name, byte[] b, int off, int len){
        return defineClass(name, b, off, len);
    }
}
