/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.api.classloader;

import com.os.falcon.network.api.message.FAEvent;
import com.os.falcon.network.api.message.FMessage;
import com.os.falcon.network.api.message.FSMessageEvent;
import com.os.falcon.network.server.api.FServerManager;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.lang.reflect.ParameterizedType;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

/**
 *
 * @author phamquan
 */
public class OSClassLoaderManager {

    private final static String USER_DIR = System.getProperty("user.dir");
    private final static String FOLDER_CLASSES = USER_DIR + File.separator + "class";
    private final static String FOLDER_TELNET = FOLDER_CLASSES + File.separator + "telnet";
    private final static String FOLDER_EVENT = FOLDER_CLASSES + File.separator + "event";
    private final static String FILE_CONF = FOLDER_CLASSES + File.separator + "class.cnf";

    private static OSClassLoaderManager _instance = null;

    public static OSClassLoaderManager getInstance() {
        if (_instance == null) {
            _instance = new OSClassLoaderManager();
            _instance.init();
        }
        return _instance;
    }

    private OSClassLoader curClassLoader = null;

    private void init() {

    }

    /**
     * Load sự kiện và cả các class liên quan (CS và SC) nằm trong cùng thư mục
     *
     * @throws java.lang.Exception
     */
    public void doLoadClass() throws Exception {
        if (curClassLoader == null) {
            curClassLoader = new OSClassLoader(OSClassLoader.class.getClassLoader());
        } else {
            curClassLoader = new OSClassLoader(curClassLoader);
        }
        File file = new File(FILE_CONF);
        BufferedReader br = new BufferedReader(new FileReader(file));

        String classPackage;
        while ((classPackage = br.readLine()) != null) {
            if (classPackage.isEmpty() || classPackage.contains("#")) {
                continue;
            }

            Class cl = loadClass(classPackage);
            Object ob = cl.newInstance();
            if (ob instanceof FSMessageEvent) {
                FSMessageEvent fEvent = (FSMessageEvent) ob;
                FAEvent event = (FAEvent) cl.getAnnotation(FAEvent.class);
                Class csClass = ((Class) ((ParameterizedType) cl.getGenericSuperclass()).getActualTypeArguments()[0]);
                FMessage fmessage = (FMessage) csClass.newInstance();
                if (FServerManager.getInstance().getActions().containsKey(fmessage.getEvent())) {
                    List<FSMessageEvent> listEvent = FServerManager.getInstance().getActions().get(fmessage.getEvent());
                    for (FSMessageEvent lEvent : listEvent){
                        if (lEvent.getClass().getName().compareTo(fEvent.getClass().getName()) == 0){
                            listEvent.remove(lEvent);
                            listEvent.add(fEvent);
                            break;
                        }
                    }
                }
            }
        }
        
        FServerManager.getInstance().sortEvents();
    }

    private Class loadClass(String classPackage) throws Exception {
        String[] z = classPackage.split("\\.");
        String path;
        if (classPackage.contains("CMD")) {
            path = FOLDER_TELNET + File.separator + z[z.length - 1] + ".class";
        } else {
            path = FOLDER_EVENT + File.separator + z[z.length - 1] + ".class";
        }
        File file = new File(path);
        URL myUrl = file.toURI().toURL();
        URLConnection connection = myUrl.openConnection();
        InputStream input = connection.getInputStream();
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int data = input.read();

        while (data != -1) {
            buffer.write(data);
            data = input.read();
        }

        input.close();

        byte[] classData = buffer.toByteArray();

        return curClassLoader.createClass(classPackage, classData, 0, classData.length);
    }

}
