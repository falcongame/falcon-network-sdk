/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.api.telnet.cmd;

import com.os.falcon.api.telnet.Command;
import com.os.falcon.network.server.api.helper.ExecuteTimeManager;
import io.netty.channel.Channel;
/**
 *
 * @author quanph
 */
public class CMDetime extends Command{

    @Override
    public String execute(Channel channel) {
        return ExecuteTimeManager.getInstance().print();
    }
    
}
