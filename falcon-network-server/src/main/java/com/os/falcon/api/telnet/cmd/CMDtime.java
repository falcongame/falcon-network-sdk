/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.api.telnet.cmd;

import com.os.falcon.api.telnet.Command;
import com.os.falcon.network.server.api.FSession;
import io.netty.channel.Channel;
/**
 *
 * @author quanph
 */
public class CMDtime extends Command{

    @Override
    public String execute(Channel channel) {
        int type = Integer.parseInt(argList.get(0));
        return FSession.printTimeCount(type);
    }
    
}
