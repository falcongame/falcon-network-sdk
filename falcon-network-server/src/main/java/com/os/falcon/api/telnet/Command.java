/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.api.telnet;

import io.netty.channel.Channel;
import java.util.ArrayList;

/**
 *
 * @author Quan
 */
public abstract class Command {

    protected ArrayList<String> argList = new ArrayList<String>();

    public Command() {

    }

    public void setArgs(ArrayList<String> argList) {
        this.argList = argList;
    }

    public abstract String execute(Channel channel);

}
