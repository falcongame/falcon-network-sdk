/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.api.telnet.cmd;

import com.os.falcon.api.telnet.Command;
import com.os.falcon.network.server.api.FSession;
import io.netty.channel.Channel;
import org.apache.log4j.net.SocketServer;
/**
 *
 * @author quanph
 */
public class CMDtimeReset extends Command{

    @Override
    public String execute(Channel channel) {
        FSession.resetTimeCount();
        return "OK";
    }
    
}
