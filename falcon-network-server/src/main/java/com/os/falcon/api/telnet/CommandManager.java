/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.api.telnet;

import com.google.common.collect.MapMaker;
import com.os.falcon.net.sdk.api.util.LogUtil;
import io.netty.channel.Channel;
import org.reflections.Reflections;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Quan
 */
public class CommandManager {

    String request = "";

    public static final String COMMAND_PREFIX = "CMD";

    private static CommandManager _instance = null;

    public static CommandManager getInstance() {
        if (_instance == null) {
            _instance = new CommandManager();
            _instance.loadAllCommands();
        }
        return _instance;
    }

    private Map<String, Class<?>> mapNameToClass = new MapMaker().makeMap();

    public void runCommands(Channel channel, String request) {
        String response = "";
        
        if (request.length() == 0) {
            response = "Please type something.";
        } else if (request.toLowerCase().equals("bye")) {
            response = "Have a good day!";
            channel.close();
        } else {
            String[] args = request.split(";");
            String commandName = args[0];
            String commandClassName = CommandManager.COMMAND_PREFIX + commandName;
            ArrayList<String> argList = new ArrayList<String>();
            for (int i = 1; i < args.length; i++) {
                argList.add(args[i]);
            }
            try {
                String classPath = commandClassName;

                Class cls = mapNameToClass.get(classPath);

                if (cls == null) {
                    response = "Unknown Command";
                } else {
                    Command command = (Command) cls.newInstance();
                    command.setArgs(argList);
                    response = command.execute(channel);
                }
            } catch (Exception ex) {
                LogUtil.error(ex);
            }
        }

        channel.writeAndFlush(response + "\r\n");
    }

    private void loadAllCommands() {
        Reflections reflections = new Reflections("com.os.falcon");
        Set<Class<? extends Command>> classes = reflections.getSubTypesOf(Command.class);
        for (Class<?> cls : classes) {
            mapNameToClass.put(cls.getSimpleName(), cls);
        }
    }

    public void addCommand(String name, Class<?> cls) {
        mapNameToClass.put(name, cls);
    }

}
