/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.network.server.api.helper;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author quanph
 */
public class ExecuteTimeManager {

    private static ExecuteTimeManager _instance = null;

    private Map<String, Map<String, Integer>> timeMap = new HashMap();

    public static ExecuteTimeManager getInstance() {
        if (_instance == null) {
            _instance = new ExecuteTimeManager();
        }
        return _instance;
    }

    void collect(ExecuteTime eTime) {
        String path = eTime.getPath();
        if (!timeMap.containsKey(path)) {
            timeMap.put(path, new HashMap());
        }
        Map<String, Integer> eTimeMap = eTime.getMapTime();
        for (String key : eTimeMap.keySet()) {
            int time = eTimeMap.get(key);
            Map<String, Integer> timePath = timeMap.get(path);
            if (!timePath.containsKey(key)) {
                timePath.put(key, 0);
            }
            timePath.put(key, timePath.get(key) + time);
        }
    }

    public String print() {
        String result = "Time Print:\n";
        for (String path : timeMap.keySet()) {
            result += path + "\n";
            Map<String, Integer> timePath = timeMap.get(path);
            int totalTime = 0;
            for (String key : timePath.keySet()) {
                int time = timePath.get(key);
                totalTime += time;
                result += "      " + key + ": " + time + "\n";
            }
            result += "  totalTime: " + totalTime + "\n";
        }

        return result;
    }
    
    public void reset(){
        timeMap.clear();
    }

    public static void main(String[] args) {
        ExecuteTime e = new ExecuteTime(ExecuteTimeManager.class.getSimpleName() + "_main");
        e.start();
        long sum = 0;
        for (int i = 0; i < 1000000000; i++) {
            sum = sum * i % 10;
        }
        e.check("test1");

        for (int i = 0; i < 2000000000; i++) {
            sum = sum * i % 10;
        }
        e.check("test2");
        for (int i = 0; i < 100000000; i++) {
            sum = sum * i % 10;
        }
        e.end();
        
        System.out.println(ExecuteTimeManager.getInstance().print());
    }
}
