/*
 * The MIT License
 *
 * Copyright 2021 quanph.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.os.falcon.network.server.api;

import com.os.falcon.net.sdk.api.util.LogUtil;
import com.os.falcon.network.api.message.CSInitSession;
import com.os.falcon.network.api.message.FMessage;
import com.os.falcon.network.api.message.SCInitSession;
import java.net.InetAddress;

/**
 *
 * @author quanph
 */
public abstract class FChannel {

    private FServer server;
    private FSession session;

    public abstract void sendMessage(FMessage message);

    public abstract String getIp();

    public abstract InetAddress getInetAddress();

    public abstract boolean isActive();
    
    public abstract void disconnect();

    public void setServer(FServer server) {
        this.server = server;
    }

    public void setSession(FSession session) {
        this.session = session;
    }

    public FSession getSession() {
        return session;
    }

    public FServer getServer() {
        return server;
    }

    public void onMessage(FMessage message) {
        LogUtil.debug(message);
        if (message instanceof CSInitSession) {
            CSInitSession csInitSession = (CSInitSession) message;
            String sessionId = csInitSession.getSessionId();
            final FSession session = FSessionManager.getInstance().getSessionById(sessionId);
            if (session != null) {
                this.session = session;
                FChannel oldChannel = session.getChannel();
                session.setChannel(this);
                session.send(new SCInitSession(SCInitSession.STATE_OLD_SESSION));
                session.restoreSession(oldChannel);
                session.getServer().getListeners().forEach((t) -> {
                    t.onContinueSession(session);
                });
            } else {
                FSession newSession = new FSession(server, this, sessionId);
                this.session = newSession;
                FSessionManager.getInstance().add(newSession);
                newSession.send(new SCInitSession(SCInitSession.STATE_NEW_SESSION));
                this.session.getServer().getListeners().forEach((t) -> {
                    t.onNewSession(newSession);
                });
            }
        } else {
            if (session != null) {
                session.onMessage(message);
            }
        }
    }

    public void onConnect() {
        for (FServerConnectionListener listener : this.getServer().getListeners()) {
            listener.onChannelConnect(this);
        }
    }

    public void onDisconnect() {
        for (FServerConnectionListener listener : this.getServer().getListeners()) {
            listener.onChannelDisconnect(this);
        }
    }
}
