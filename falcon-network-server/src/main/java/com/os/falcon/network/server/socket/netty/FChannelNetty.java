/*
 * The MIT License
 *
 * Copyright 2020 quanph.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.os.falcon.network.server.socket.netty;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.os.falcon.network.api.message.FMessage;
import com.os.falcon.network.server.api.FChannel;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import java.net.InetAddress;
import java.net.InetSocketAddress;

/**
 *
 * @author quanph
 */
public class FChannelNetty extends FChannel {

    private ChannelHandlerContext channel;

    public FChannelNetty(ChannelHandlerContext channel) {
        this.channel = channel;
    }

    @Override
    public void sendMessage(FMessage message) {
        try {
            channel.writeAndFlush(Unpooled.wrappedBuffer((message.getEvent() + ";" + FMessage.objectMapper.writeValueAsString(message) + FDelimiters.DELIMITERS).getBytes()));
        } catch (JsonProcessingException ex) {
        }

    }

    @Override
    public String getIp() {
        return ((InetSocketAddress) channel.channel().remoteAddress()).getAddress().getHostAddress();
    }

    @Override
    public InetAddress getInetAddress() {
        return ((InetSocketAddress) channel.channel().remoteAddress()).getAddress();
    }

    @Override
    public boolean isActive() {
        return channel != null && channel.channel().isActive();
    }

    @Override
    public void disconnect() {
        channel.disconnect();
    }

}
