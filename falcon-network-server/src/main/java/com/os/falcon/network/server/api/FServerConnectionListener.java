/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.network.server.api;

import com.os.falcon.network.server.api.FSession;

/**
 *
 * @author quanph
 */
public abstract class FServerConnectionListener {
    public abstract void onChannelConnect(FChannel channel);
    public abstract void onChannelDisconnect(FChannel channel);
    public abstract void onNewSession(FSession session);
    public abstract void onContinueSession(FSession session);
    public abstract void onSessionTimeout(FSession session);
}
