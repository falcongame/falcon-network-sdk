/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.network.server.api;

import com.os.falcon.network.server.api.helper.FSConstants;
import com.google.common.collect.MapMaker;
import com.os.falcon.net.sdk.api.util.LogUtil;
import com.os.falcon.network.api.message.FAEvent;
import com.os.falcon.network.api.message.FMessage;
import com.os.falcon.network.api.message.FSMessageEvent;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.reflections.Reflections;

/**
 *
 * @author quanph
 */
public class FServerManager {

    Map<String, List<FSMessageEvent>> mapEventToListAction = new MapMaker().makeMap();
    Map<String, Class> mapEventToClass = new MapMaker().makeMap();

    List<FServer> servers = new ArrayList();
    private static FServerManager _instance = null;

    public static FServerManager getInstance() {
        if (_instance == null) {
            _instance = new FServerManager();
            try {
                _instance.init();
            } catch (Exception ex) {
                LogUtil.error(ex);
            }
        }
        return _instance;
    }

    public FServer addServer(FServer server) {
        servers.add(server);
        return server;
    }

    private void init() throws InstantiationException, IllegalAccessException {
        Reflections reflections = new Reflections(FSConstants.PACKAGE_TO_SCAN);
        Map<Class, String> mapClassToEvent = new MapMaker().makeMap();

        Set<Class<? extends FMessage>> setClass = reflections.getSubTypesOf(FMessage.class);
        for (Class cls : setClass) {
            FMessage fmessage = (FMessage) cls.newInstance();
            mapEventToClass.put(fmessage.getEvent(), cls);
            mapClassToEvent.put(cls, fmessage.getEvent());
        }

        Set<Class<? extends FSMessageEvent>> eventClass = reflections.getSubTypesOf(FSMessageEvent.class);
        for (Class cls : eventClass) {
            Class csClass = ((Class) ((ParameterizedType) cls.getGenericSuperclass()).getActualTypeArguments()[0]);
            String csEventName = mapClassToEvent.get(csClass);
            if (!mapEventToListAction.containsKey(csEventName)) {
                mapEventToListAction.put(csEventName, new ArrayList<>());
                LogUtil.info("Event: " + csEventName);
            }
            mapEventToListAction.get(csEventName).add((FSMessageEvent) cls.newInstance());
        }

        sortEvents();
    }

    public Map<String, List<FSMessageEvent>> getActions() {
        return mapEventToListAction;
    }

    public void start() throws Exception {

        for (FServer server : servers) {
            server.start();
            LogUtil.info(server.getServerInfo() + " !!!");
        }
    }

    public void sortEvents() {
        for (String key : mapEventToListAction.keySet()) {
            List<FSMessageEvent> listActions = mapEventToListAction.get(key);
            listActions.sort((o1, o2) -> {
                FAEvent fAEvent1 = (FAEvent) o1.getClass().getAnnotation(FAEvent.class);
                FAEvent fAEvent2 = (FAEvent) o2.getClass().getAnnotation(FAEvent.class);
                return fAEvent1.priority() - fAEvent2.priority();
            });
        }
    }

    public Class getMessageClass(String event) {
        if (mapEventToClass.containsKey(event)) {
            return mapEventToClass.get(event);
        } else {
            LogUtil.debug("no event " + event);
            return null;
        }
    }

}
