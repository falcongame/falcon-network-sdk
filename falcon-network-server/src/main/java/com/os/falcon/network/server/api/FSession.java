/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.network.server.api;

import com.google.common.collect.EvictingQueue;
import com.os.falcon.net.sdk.api.util.LogUtil;
import com.os.falcon.network.api.future.FFuture;
import com.os.falcon.network.api.future.FFutureManager;
import com.os.falcon.network.api.message.CSInitSession;
import com.os.falcon.network.api.message.CSSCResponse;
import com.os.falcon.network.api.message.FMessage;
import com.os.falcon.network.api.message.FPing;
import com.os.falcon.network.api.message.FPong;
import com.os.falcon.network.api.message.FSMessageEvent;
import com.os.falcon.network.api.message.SCCloseSession;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;

/**
 *
 * @author quanph
 */
public class FSession {

    private FServer server;
    private FChannel channel;
    private String sessionId;
    private long maxCSSequence;
    private long maxSCSequence;

    private long lastTimeReceivedMessage = System.currentTimeMillis();
    private Queue<FMessage> messageQueue = EvictingQueue.create(1_000);

    public FSession(FServer server, FChannel channel, String sessionId) {
        this.server = server;
        this.channel = channel;
        this.sessionId = sessionId;
    }

    public void send(FMessage message) {
        send(message, false);
    }

    public void send(FMessage message, boolean important) {
        message = message.clone();
        message.setTimeServer(System.currentTimeMillis());
        if (message instanceof FPong) {
            channel.sendMessage(message);
            return;
        }

        message.setCsSequence(maxCSSequence);
        message.setScSequence(maxSCSequence++);

        if (important) {
            messageQueue.add(message);
        }
        channel.sendMessage(message);
        LogUtil.debug(message);
    }

    public FFuture send(FMessage message, FFuture future, int timeOut) {
        if (future == FFuture.RECEIVED) {
            message.setResponse(1);
        } else if (future == FFuture.PROCESSED) {
            message.setResponse(2);
        }
        String messageId = UUID.randomUUID().toString();
        message.setMessageId(messageId);
        send(message);
        FFuture fFuture = new FFuture(messageId, timeOut);
        FFutureManager.getInstance().put(messageId, fFuture);
        return fFuture;
    }

    public FFuture send(FMessage message, FFuture future) {
        return send(message, future, 5);
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public FServer getServer() {
        return server;
    }

    public void setServer(FServer server) {
        this.server = server;
    }

    public FChannel getClient() {
        return channel;
    }

    public void setClient(FChannel client) {
        this.channel = client;
    }

    public void restoreSession(FChannel oldChannel) {
        while (!messageQueue.isEmpty()) {
            FMessage m = messageQueue.poll();
            channel.sendMessage(m);
        }
    }

    private static Map<String, Integer> event2Time = new HashMap();
    private static Map<String, Integer> event2Count = new HashMap();

    public void onMessage(FMessage message) {
        message.setTimeServer(System.currentTimeMillis());
        lastTimeReceivedMessage = System.currentTimeMillis();
        if (message instanceof FPing) {
            channel.sendMessage(new FPong());
            return;
        }
//        if (maxCSSequence + 1 != message.getCsSequence() && !(message instanceof CSInitSession)) {
//            return;
//        }
        if (!(message instanceof CSInitSession)) {
            maxCSSequence = message.getCsSequence();
            long scSequence = message.getScSequence();
            while (!messageQueue.isEmpty()) {
                FMessage m = messageQueue.peek();
                if (m.getScSequence() <= scSequence) {
                    messageQueue.poll();
                } else {
                    break;
                }
            }
        }

        if (message.getResponse() == 1) {
            this.send(new CSSCResponse(message.getMessageId()));
        }
        if (FServerManager.getInstance().getActions().containsKey(message.getEvent())) {
            FServerManager.getInstance().getActions().get(message.getEvent()).forEach((FSMessageEvent fEvent) -> {
                String key = fEvent.getClass().getSimpleName();
                if (!event2Count.containsKey(key)) {
                    event2Count.put(key, 0);
                }
                event2Count.put(key, event2Count.get(key) + 1);

                long beforeTime = System.currentTimeMillis();
                fEvent.onMessage(this, message);
                int deltaTime = (int) (System.currentTimeMillis() - beforeTime);
                if (!event2Time.containsKey(key)) {
                    event2Time.put(key, 0);
                }
                event2Time.put(key, event2Time.get(key) + deltaTime);
            });
        }

        if (message.getResponse() == 2) {
            this.send(new CSSCResponse(message.getMessageId()));
        }

    }

    public boolean isActive() {
        return channel.isActive();
    }

    public long getLastTimeReceivedMessage() {
        return lastTimeReceivedMessage;
    }

    public FChannel getChannel() {
        return channel;
    }

    public void setChannel(FChannel channel) {
        this.channel = channel;
    }

    public void close() {
        this.send(new SCCloseSession());
        FSessionManager.getInstance().remove(this);
        messageQueue.clear();
    }

    ////////////////////////////////////////////////////////////////////////////
    //                           STATIC METHODS                               //   
    ////////////////////////////////////////////////////////////////////////////
    public static void resetTimeCount() {
        event2Count.clear();
        event2Time.clear();
    }

    public static String printTimeCount(int type) {
        String result = "Time Calculate:\n";
        int sumTime = 0;
        for (Map.Entry<String, Integer> entry : event2Time.entrySet()) {
            sumTime += entry.getValue();
        }
        result += "sumTime: " + sumTime + "\n";

        List<Map.Entry<String, Integer>> list;
        switch (type) {
            case 1:
                list = new ArrayList<>(event2Time.entrySet());
                list.sort(Map.Entry.comparingByValue());
                Collections.reverse(list);
                for (Map.Entry<String, Integer> entry : list) {
                    try {
                        int totalTime = entry.getValue();
                        int count = event2Count.get(entry.getKey());
                        result += entry.getKey() + ": " + String.format("totalTime = %d, count = %d, avg = %d \n", totalTime, count, (int) (totalTime / count));
                    } catch (Exception ex) {

                    }
                }
                break;
            case 2:
                list = new ArrayList<>(event2Count.entrySet());
                list.sort(Map.Entry.comparingByValue());
                Collections.reverse(list);
                for (Map.Entry<String, Integer> entry : list) {
                    try {
                        int count = entry.getValue();
                        int totalTime = event2Time.get(entry.getKey());
                        result += entry.getKey() + ": " + String.format("totalTime = %d, count = %d, avg = %d \n", totalTime, count, (int) (totalTime / count));
                    } catch (Exception ex) {

                    }
                }
                break;
            case 3:
                Map<String, Integer> event2Avg = new HashMap();
                List<Map.Entry<String, Integer>> list2 = new ArrayList<>(event2Count.entrySet());
                for (Map.Entry<String, Integer> entry : list2) {
                    try {
                        int count = entry.getValue();
                        int totalTime = event2Time.get(entry.getKey());
                        event2Avg.put(entry.getKey(), (totalTime / count));
                    } catch (Exception ex) {

                    }
                }
                list = new ArrayList<>(event2Avg.entrySet());
                list.sort(Map.Entry.comparingByValue());
                Collections.reverse(list);
                for (Map.Entry<String, Integer> entry : list) {
                    try {
                        int avg = entry.getValue();
                        int totalTime = event2Time.get(entry.getKey());
                        int count = event2Count.get(entry.getKey());
                        result += entry.getKey() + ": " + String.format("totalTime = %d, count = %d, avg = %d \n", totalTime, count, avg);
                    } catch (Exception ex) {

                    }
                }
                break;
            default:
                break;
        }

        return result;

    }
}
