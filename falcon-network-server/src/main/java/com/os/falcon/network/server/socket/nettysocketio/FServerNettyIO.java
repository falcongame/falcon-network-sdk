/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.network.server.socket.nettysocketio;

import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.Transport;
import com.google.common.collect.MapMaker;
import com.os.falcon.network.api.message.CSInitSession;
import com.os.falcon.network.api.message.FMessage;
import com.os.falcon.network.api.message.FPing;
import com.os.falcon.network.api.message.FSMessageEvent;
import com.os.falcon.network.server.api.FServer;
import com.os.falcon.network.server.api.FServerManager;
import java.io.FileInputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

/**
 *
 * @author quanph
 */
public class FServerNettyIO extends FServer {

    private Map<SocketIOClient, FChannelNettyIO> mapNettyIOClientToFClientSocket = new MapMaker().makeMap();

    public FServerNettyIO(int port, String context) {
        this.port = port;
        this.context = context;
        this.serverInfo = "FServerNettyIO started at http" + (ssl ? "s" : "") + "://localhost:" + port;
    }

    SocketIOServer server;
    private int port;
    private String context;
    private boolean ssl;
    private String sslFilePath;
    private String sslKeyPass;

    @Override
    public void start() {
        Configuration configuration = new Configuration();
        configuration.setHostname("0.0.0.0");
        configuration.setPort(port);
        configuration.setContext(context);
        configuration.setTransports(Transport.WEBSOCKET, Transport.POLLING);
        configuration.getSocketConfig().setReuseAddress(true);
        if (ssl) {
            try {
                configuration.setKeyStore(new FileInputStream(sslFilePath));
                configuration.setKeyStorePassword(sslKeyPass);
            } catch (Exception xxx) {
                xxx.printStackTrace();
            }
        }
        server = new SocketIOServer(configuration);
        server.addConnectListener((sioc) -> {
            FChannelNettyIO channel = new FChannelNettyIO(sioc);
            mapNettyIOClientToFClientSocket.put(sioc, channel);
            channel.setServer(this);
            channel.onConnect();
        });
        server.addDisconnectListener((sioc) -> {
            FChannelNettyIO channel = mapNettyIOClientToFClientSocket.get(sioc);
            channel.onDisconnect();
            mapNettyIOClientToFClientSocket.remove(sioc);
            
        });

        for (String event : FServerManager.getInstance().getActions().keySet()) {
            for (FSMessageEvent fEvent : FServerManager.getInstance().getActions().get(event)) {
                Type genericSuperClass = fEvent.getClass().getGenericSuperclass();
                if (genericSuperClass instanceof ParameterizedType) {
                    Type[] genericTypes = ((ParameterizedType) genericSuperClass).getActualTypeArguments();
                    Class csMessageClass = (Class) genericTypes[0];

                    server.addEventListener(event, csMessageClass, (sioc, t, ar) -> {
                        try {
                            mapNettyIOClientToFClientSocket.get(sioc).onMessage((FMessage) t);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    });
                }
            }

        }

        server.addEventListener("f_ping", FPing.class, (sioc, t, ar) -> {
            try {
                mapNettyIOClientToFClientSocket.get(sioc).onMessage((FMessage) t);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        server.addEventListener("f_cs_init_session", CSInitSession.class, (sioc, t, ar) -> {
            try {
                mapNettyIOClientToFClientSocket.get(sioc).onMessage((FMessage) t);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        server.start();
    }

    public FServerNettyIO setSSL(String sslFilePath, String sslKeyPass) {
        this.ssl = true;
        this.sslFilePath = sslFilePath;
        this.sslKeyPass = sslKeyPass;
        return this;
    }

    @Override
    public void stop() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
