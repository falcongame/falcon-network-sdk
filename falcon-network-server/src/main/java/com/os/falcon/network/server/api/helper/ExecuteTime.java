/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.network.server.api.helper;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author quanph
 */
public class ExecuteTime {

    private String path;
    private int count = 0;
    private long startTime = 0l;
    private long lastTime = 0l;
    private Map<String, Integer> mapTime = new HashMap();
    public ExecuteTime(String path) {
        this.path = path;
    }
    
    public void start(){
        startTime = System.currentTimeMillis();
        lastTime = startTime;
    }
    
    public void check(String note){
        int time = (int)(System.currentTimeMillis() - lastTime);
        lastTime = System.currentTimeMillis();
        mapTime.put(note, time);
    }
    
    public void end(){
        check("end");
        ExecuteTimeManager.getInstance().collect(this);
    }

    public String getPath() {
        return path;
    }

    public Map<String, Integer> getMapTime() {
        return mapTime;
    }

}
