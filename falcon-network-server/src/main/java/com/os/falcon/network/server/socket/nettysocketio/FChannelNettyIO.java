/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.network.server.socket.nettysocketio;

import com.corundumstudio.socketio.SocketIOClient;
import com.os.falcon.network.api.message.FMessage;
import com.os.falcon.network.server.api.FChannel;
import java.net.InetAddress;
import java.net.InetSocketAddress;

/**
 *
 * @author quanph
 */
public class FChannelNettyIO extends FChannel {

    private SocketIOClient socketClient;

    public FChannelNettyIO() {
    }

    public FChannelNettyIO(SocketIOClient socketClient) {
        this.socketClient = socketClient;
    }

    @Override
    public void sendMessage(FMessage message) {
        socketClient.sendEvent(message.getEvent(), message);
    }

    @Override
    public String getIp() {
        return ((InetSocketAddress) socketClient.getRemoteAddress()).getAddress().getHostAddress();
    }

    @Override
    public InetAddress getInetAddress() {
        return ((InetSocketAddress) socketClient.getRemoteAddress()).getAddress();
    }

    @Override
    public boolean isActive() {
        return socketClient != null && socketClient.isChannelOpen();
    }

    @Override
    public void disconnect() {
        socketClient.disconnect();
    }

}
