/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.network.server.api;

import com.google.common.collect.MapMaker;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author quanph
 */
public abstract class FServer {

    private Set<FServerConnectionListener> setListeners = new HashSet<FServerConnectionListener>();
    private Map<String, FSession> mapSessionToClientSocket = new MapMaker().makeMap();
    protected String serverInfo;

    public void addConnectionListener(FServerConnectionListener listener) {
        setListeners.add(listener);
    }

    public Set<FServerConnectionListener> getListeners() {
        return setListeners;
    }

    public abstract void start() throws Exception;

    public abstract void stop() throws Exception;

    public Map<String, FSession> getMapSessionToClientSocket() {
        return mapSessionToClientSocket;
    }

    public String getServerInfo() {
        return serverInfo;
    }

    private int timeOut = 300;

    public void setTimeout(int timeOut) {
        this.timeOut = timeOut;
    }

    public int getTimeout() {
        return this.timeOut;
    }

}
