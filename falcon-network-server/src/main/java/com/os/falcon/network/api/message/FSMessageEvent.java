/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.network.api.message;

import com.os.falcon.network.server.api.FSession;

/**
 *
 * @author quanph
 */
public abstract class FSMessageEvent <T extends Object>{
    public abstract void onMessage(FSession session, T message);
}
