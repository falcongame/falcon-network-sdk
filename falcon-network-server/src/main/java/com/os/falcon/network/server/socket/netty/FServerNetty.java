/*
 * The MIT License
 *
 * Copyright 2020 quanph.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.os.falcon.network.server.socket.netty;

import com.google.common.collect.MapMaker;
import com.os.falcon.network.server.api.FServer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import java.util.Map;

/**
 *
 * @author quanph
 */
public class FServerNetty extends FServer {

    private int port;
    ServerBootstrap b;
    NioEventLoopGroup masterGroup;
    NioEventLoopGroup workerGroup;
    private Map<ChannelHandlerContext, FChannelNetty> mapChannelToClientNetty = new MapMaker().makeMap();

    public FServerNetty(int port) {
        this.port = port;
        this.serverInfo = "FServerNetty at port " + port;
    }

    @Override
    public void start() throws Exception {
            masterGroup = new NioEventLoopGroup(2); // 2 threads
            workerGroup = new NioEventLoopGroup(10); // 10 threads
            b = new ServerBootstrap();
            b.group(masterGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new FNettySocketInitialiser(this))
                    .option(ChannelOption.SO_BACKLOG, 5)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            b.localAddress(port).bind().sync();
    }

    public FChannelNetty getClientSocketNetty(ChannelHandlerContext ch) {
        if (!mapChannelToClientNetty.containsKey(ch)) {
            FChannelNetty fClientSocketNetty = new FChannelNetty(ch);
            fClientSocketNetty.setServer(this);
            mapChannelToClientNetty.put(ch, fClientSocketNetty);
        }
        return mapChannelToClientNetty.get(ch);
    }

    @Override
    public void stop() throws Exception {
        masterGroup.shutdownGracefully();
        workerGroup.shutdownGracefully();
    }
}
