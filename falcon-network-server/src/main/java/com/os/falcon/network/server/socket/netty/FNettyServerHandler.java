/*
 * The MIT License
 *
 * Copyright 2020 quanph.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.os.falcon.network.server.socket.netty;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.os.falcon.net.sdk.api.util.LogUtil;
import com.os.falcon.network.api.message.FMessage;
import com.os.falcon.network.server.api.FChannel;
import com.os.falcon.network.server.api.FServerManager;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.io.IOException;

/**
 *
 * @author quanph
 */
public class FNettyServerHandler extends SimpleChannelInboundHandler<String> {

    FServerNetty fServer;

    FNettyServerHandler(FServerNetty fServer) {
        this.fServer = fServer;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext chc, String content) {
        try {
            int firstCommaIndex = content.indexOf(";");
            String event = content.substring(0, firstCommaIndex);
            String jsonContent = content.substring(firstCommaIndex + 1);
            Class cls = FServerManager.getInstance().getMessageClass(event);
            if (cls != null) {
                Object message = FMessage.objectMapper.readValue(jsonContent, cls);
                fServer.getClientSocketNetty(chc).onMessage((FMessage) message);
            }

        } catch (IOException ex) {
            LogUtil.error(ex);
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx); //To change body of generated methods, choose Tools | Templates.
        FChannel channel = fServer.getClientSocketNetty(ctx);
        channel.onDisconnect();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx); //To change body of generated methods, choose Tools | Templates.
        FChannel channel = fServer.getClientSocketNetty(ctx);
        channel.onConnect();
    }

    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        LogUtil.error(cause);
    }
}
