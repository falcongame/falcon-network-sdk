/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.network.client.socket.socketio;

import com.os.falcon.net.sdk.api.util.LogUtil;
import com.os.falcon.network.client.api.common.FCMessageEvent;
import com.os.falcon.network.api.message.FMessage;
import com.os.falcon.network.api.message.FPong;
import com.os.falcon.network.api.message.SCInitSession;
import com.os.falcon.network.client.api.common.FClientManager;
import com.os.falcon.network.client.api.session.FChannel;
import com.os.falcon.network.client.api.session.FSession;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.transports.WebSocket;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author quanph
 */
public class FSocketIOChannel extends FChannel {

    private Socket socket;
    private String uri;

    public FSocketIOChannel(String uri) {
        this.uri = uri;
    }
    
    public FSocketIOChannel(FSession session, String uri) {
            super.setSession(session);
        this.uri = uri;
    }

    @Override
    public void start() {
        try {
            String contextPath = this.uri.substring(this.uri.lastIndexOf("/"));
            String xUri = this.uri.substring(0, this.uri.lastIndexOf("/"));
            IO.Options opts = new IO.Options();
            opts.forceNew = true;
            opts.path = contextPath;
            opts.reconnection = true;
            opts.transports = new String[]{WebSocket.NAME};

            socket = IO.socket(xUri, opts);

            for (String event : FClientManager.getInstance().getActions().keySet()) {
                for (FCMessageEvent mEvent : FClientManager.getInstance().getActions().get(event)) {
                    socket.on(event, (os) -> {
                        Type genericSuperClass = mEvent.getClass().getGenericSuperclass();
                        if (genericSuperClass instanceof ParameterizedType) {
                            Type[] genericTypes = ((ParameterizedType) genericSuperClass).getActualTypeArguments();
                            Class scMessageClass = (Class) genericTypes[0];
                            try {
                                Object message = FMessage.objectMapper.readValue(os[0].toString(), scMessageClass);
                                this.onMessage((FMessage) message);
//                            mEvent.onMessage(this, message);
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }

                        }
                    });
                }
            }

            socket.on("f_pong", (os) -> {
                try {
                    Object message = FMessage.objectMapper.readValue(os[0].toString(), FPong.class);
                    FPong fPong = (FPong) message;
                    this.onMessage(fPong);
                } catch (IOException ex) {
                    Logger.getLogger(FSocketIOChannel.class.getName()).log(Level.SEVERE, null, ex);
                }
            });

            socket.on("f_sc_init_session", (os) -> {
                try {
                    Object message = FMessage.objectMapper.readValue(os[0].toString(), SCInitSession.class);
                    SCInitSession scInitSession = (SCInitSession) message;
                    this.onMessage(scInitSession);
                } catch (IOException ex) {
                    Logger.getLogger(FSocketIOChannel.class.getName()).log(Level.SEVERE, null, ex);
                }

            });

            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... os) {
                    FSocketIOChannel.this.onConnected();
                }
            }).on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
                @Override
                public void call(Object... os) {
                    FSocketIOChannel.this.onDisconnected();
                }
            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... os) {
                    FSocketIOChannel.this.onDisconnected();
                }
            });

            socket.connect();
        } catch (Exception ex) {
            LogUtil.error(ex);
        }
    }

    @Override
    public void sendMessage(FMessage message) {
        try {
            
            socket.emit(message.getEvent(), new JSONObject(message, true));
        } catch (Exception ex) {
            Logger.getLogger(FSocketIOChannel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void reconnect() {
    }

    @Override
    public void disconnect() {
        socket.disconnect();
    }

    public FSocketIOChannel clone(){
        FSocketIOChannel newChannel = new FSocketIOChannel(this.getSession(), uri);
        this.getSession().setChannel(newChannel);
        return newChannel;
    }
}
