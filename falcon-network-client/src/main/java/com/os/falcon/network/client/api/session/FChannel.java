/*
 * The MIT License
 *
 * Copyright 2021 quanph.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.os.falcon.network.client.api.session;

import com.os.falcon.net.sdk.api.util.LogUtil;
import com.os.falcon.network.api.message.FMessage;
import com.os.falcon.network.api.message.SCInitSession;

/**
 *
 * @author quanph
 */
public abstract class FChannel {

    private FSession session;

    public abstract void start();

    public abstract void reconnect();
    
    public abstract void disconnect();
    
    public abstract FChannel clone();

    protected abstract void sendMessage(FMessage message);

    public void send(FMessage message) {
        LogUtil.debug(message);
        sendMessage(message);
    }

    public void onConnected() {
        session.onChannelConnected();
    }

    ;
    public void onDisconnected() {
        session.onChannelDisconnected();
    }

    public FSession getSession() {
        return session;
    }

    public void setSession(FSession session) {
        this.session = session;
    }

    public void onMessage(FMessage message) {
        if (message instanceof SCInitSession) {
            SCInitSession initSession = (SCInitSession) message;
            if (initSession.getState() == SCInitSession.STATE_NEW_SESSION) {
                session.newSession();
            } else {
                session.continueCurrentSession();
            }
        } else {
            session.onMessage(message);
        }
    }
}
