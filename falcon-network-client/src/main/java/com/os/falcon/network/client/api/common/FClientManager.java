/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.network.client.api.common;

import com.google.common.collect.MapMaker;
import com.os.falcon.network.api.message.FAEvent;
import com.os.falcon.network.api.message.FMessage;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.reflections.Reflections;

/**
 *
 * @author quanph
 */
public class FClientManager {

    Map<String, List<FCMessageEvent>> mapEventToListAction = new MapMaker().makeMap();
    private static FClientManager _instance = null;
    Map<String, Class> mapEventToClass = new MapMaker().makeMap();

    public static FClientManager getInstance() {
        if (_instance == null) {
            _instance = new FClientManager();
            try {
                _instance.init();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return _instance;
    }

    private void init() throws Exception {
        Reflections reflections = new Reflections(FCConstants.PACKAGE_TO_SCAN);
        Map<Class, String> mapClassToEvent = new MapMaker().makeMap();
        
        Set<Class<? extends FMessage>> setClass = reflections.getSubTypesOf(FMessage.class);
        for (Class cls : setClass) {
            System.out.println(cls.getName());
            FMessage message = (FMessage) cls.newInstance();
            mapEventToClass.put(message.getEvent(), cls);
            mapClassToEvent.put(cls, message.getEvent());
        }
        
//        Set<Class<?>> eventClass = reflections.getTypesAnnotatedWith(FAEvent.class);
        Set<Class<? extends FCMessageEvent>> eventClass = reflections.getSubTypesOf(FCMessageEvent.class);
        for (Class cls : eventClass) {
            Class csClass = ((Class) ((ParameterizedType) cls.getGenericSuperclass()).getActualTypeArguments()[0]);
            String csEventName = mapClassToEvent.get(csClass);
            if (!mapEventToListAction.containsKey(csEventName)) {
                mapEventToListAction.put(csEventName, new ArrayList<>());
            }
            mapEventToListAction.get(csEventName).add((FCMessageEvent) cls.newInstance());
        }

        for (List<FCMessageEvent> listActions : mapEventToListAction.values()) {
            listActions.sort((o1, o2) -> {
                FAEvent fAEvent1 = (FAEvent) o1.getClass().getAnnotation(FAEvent.class);
                FAEvent fAEvent2 = (FAEvent) o1.getClass().getAnnotation(FAEvent.class);
                return fAEvent1.priority() - fAEvent2.priority();
            });
        }

    }


    public Map<String, List<FCMessageEvent>> getActions() {
        return mapEventToListAction;
    }

    public Class getMessageClass(String event) {
        if (mapEventToClass.containsKey(event)) {
            return mapEventToClass.get(event);
        } else {
            return null;
        }
    }

}
