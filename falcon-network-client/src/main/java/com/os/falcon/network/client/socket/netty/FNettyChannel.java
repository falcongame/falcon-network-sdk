/*
 * The MIT License
 *
 * Copyright 2020 quanph.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.os.falcon.network.client.socket.netty;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.os.falcon.network.api.message.FMessage;
import com.os.falcon.network.client.api.session.FChannel;
import com.os.falcon.network.client.api.session.FSession;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author quanph
 */
public class FNettyChannel extends FChannel {

    private static int i = 0;
    String host;
    int port;
    ChannelHandlerContext nettyChannelHandlerContext;
    Executor executor = Executors.newSingleThreadExecutor();

    public FNettyChannel(String host, int port) {
        this.host = host;
        this.port = port;
    }
    
    public FNettyChannel(FSession session, String host, int port) {
        super.setSession(session);
        this.host = host;
        this.port = port;
    }

    public void setNettyChannelHandlerContext(ChannelHandlerContext nettyChannelHandlerContext) {
        this.nettyChannelHandlerContext = nettyChannelHandlerContext;
    }

    @Override
    public void sendMessage(FMessage message) {
        if (nettyChannelHandlerContext != null && nettyChannelHandlerContext.channel() != null && nettyChannelHandlerContext.channel().isWritable()) {
            try {
                nettyChannelHandlerContext.writeAndFlush(Unpooled.wrappedBuffer((message.getEvent() + ";" + FMessage.objectMapper.writeValueAsString(message) + FDelimiters.DELIMITERS).getBytes()));
            } catch (JsonProcessingException ex) {
            }
        }
    }

    @Override
    public void start() {
        executor.execute(() -> {
            EventLoopGroup workerGroup = new NioEventLoopGroup(10);
            try {
                Bootstrap b = new Bootstrap(); // (1)
                b.group(workerGroup); // (2)
                b.channel(NioSocketChannel.class); // (3)
                b.option(ChannelOption.SO_KEEPALIVE, true); // (4)
                b.handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    public void initChannel(SocketChannel ch) throws Exception {
                        ch.pipeline().addLast("framer", new DelimiterBasedFrameDecoder(18192, new ByteBuf[]{
                            Unpooled.wrappedBuffer(FDelimiters.DELIMITERS.getBytes())
                        }));
                        ch.pipeline().addLast("encoder", new StringEncoder());
                        ch.pipeline().addLast("decoder", new StringDecoder());
                        ch.pipeline().addLast("handler", new FNettyChannelHandler(FNettyChannel.this));

                    }

                });

                ChannelFuture f = b.connect(host, port).addListener(new FNettyConnectionListener(this));

                f.channel().closeFuture().sync();
            } catch (InterruptedException ex) {

            } finally {
                workerGroup.shutdownGracefully();
            }
        });
    }

    @Override
    public void reconnect() {
        nettyChannelHandlerContext.disconnect();
        start();
    }

    @Override
    public void disconnect() {
        nettyChannelHandlerContext.disconnect();
    }
    
    public FNettyChannel clone(){
        FNettyChannel newChannel = new FNettyChannel(this.getSession(), host, port);
        this.getSession().setChannel(newChannel);
        return newChannel;
    }

}
