/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.os.falcon.network.client.api.common;

import com.os.falcon.network.client.api.session.FSession;

/**
 *
 * @author quanph
 */
public abstract class FCMessageEvent <T extends Object>{
    public abstract void onMessage(FSession session, T message);
}
