/*
 * The MIT License
 *
 * Copyright 2020 quanph.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.os.falcon.network.client.socket.netty;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.os.falcon.network.api.message.FMessage;
import com.os.falcon.network.client.api.common.FClientManager;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 *
 * @author quanph
 */
public class FNettyChannelHandler extends SimpleChannelInboundHandler<String> {

    private FNettyChannel channel;

    public FNettyChannelHandler(FNettyChannel channel) {
        this.channel = channel;
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx); //To change body of generated methods, choose Tools | Templates.
        channel.onDisconnected();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx); //To change body of generated methods, choose Tools | Templates.
        channel.setNettyChannelHandlerContext(ctx);
        channel.onConnected();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        ctx.close();
        channel.onDisconnected();
    }
    
    

    @Override
    protected void channelRead0(ChannelHandlerContext chc, String content) throws Exception {
        int firstCommaIndex = content.indexOf(";");
        String event = content.substring(0, firstCommaIndex);
        String jsonContent = content.substring(firstCommaIndex + 1);
        Class cls = FClientManager.getInstance().getMessageClass(event);
        if (cls != null) {
            Object message = FMessage.objectMapper.readValue(jsonContent, cls);
            channel.onMessage((FMessage)message);
        }
    }
}
