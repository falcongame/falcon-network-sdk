
/*
 * The MIT License
 *
 * Copyright 2021 quanph.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.os.falcon.network.client.api.session;

import com.google.common.collect.EvictingQueue;
import com.os.falcon.net.sdk.api.util.LogUtil;
import com.os.falcon.network.api.future.FFuture;
import com.os.falcon.network.api.future.FFutureManager;
import com.os.falcon.network.client.api.common.FCMessageEvent;
import com.os.falcon.network.api.message.FMessage;
import com.os.falcon.network.api.message.FPing;
import com.os.falcon.network.api.message.CSInitSession;
import com.os.falcon.network.api.message.CSSCResponse;
import com.os.falcon.network.api.message.SCCloseSession;
import com.os.falcon.network.client.api.common.FClientManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author quanph
 */
public class FSession implements ISession {

    private FChannel channel;
    private List<ISessionListener> listeners = new ArrayList();
    private String sessionId;
    private boolean isStarted = false;
    private boolean forceDisconnected = false;

    private long maxCSSequence;
    private long maxSCSequence;

    private int firstSession = 0;

    private Queue<FMessage> importantQueue = EvictingQueue.create(1_000);
    private final ScheduledExecutorService scheduler
            = Executors.newScheduledThreadPool(1);

    public FSession(FChannel channel) {
        this.channel = channel;
        this.channel.setSession(this);
        UUID uuid = UUID.randomUUID();
        this.sessionId = uuid.toString();
    }

    public FChannel getChannel() {
        return channel;
    }

    public void setChannel(FChannel channel) {
        this.channel = channel;
    }

    public List<ISessionListener> getListeners() {
        return listeners;
    }

    public String getSessionId() {
        return sessionId;
    }

    public boolean isActive() {
        return isStarted;
    }

    @Override
    public void onChannelConnected() {
        LogUtil.info("Channel connected!");
        for (ISessionListener listener : listeners) {
            listener.onChannelConnected(this);
        }
        channel.send(new CSInitSession(sessionId));
    }

    @Override
    public void onChannelDisconnected() {
        LogUtil.info("Channel disconnected!");
        for (ISessionListener listener : listeners) {
            listener.onChannelDisconnected(this);
        }
        if (!this.forceDisconnected) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {

            }
            channel.start();
        }
    }

    @Override
    public void onMessage(FMessage message) {
        LogUtil.debug(message);
        message.setTimeClient(System.currentTimeMillis());
        if (message instanceof SCCloseSession) {
            this.forceDisconnected = true;
            channel.disconnect();
        } else if (maxSCSequence < message.getScSequence()) {
            maxSCSequence = message.getScSequence();
            long csSequence = message.getCsSequence();
            while (!importantQueue.isEmpty()) {
                FMessage m = importantQueue.peek();
                if (m.getCsSequence() <= csSequence) {
                    importantQueue.poll();
                } else {
                    break;
                }
            }

            if (message.getResponse() == 1) {
                this.send(new CSSCResponse(message.getMessageId()));
            }
            if (FClientManager.getInstance().getActions().containsKey(message.getEvent())) {
                for (FCMessageEvent event : FClientManager.getInstance().getActions().get(message.getEvent())) {
                    event.onMessage(this, message);
                }
            }
            if (message.getResponse() == 2) {
                this.send(new CSSCResponse(message.getMessageId()));
            }
        }

    }

    @Override
    public void send(FMessage message) {
        send(message, false);
    }

    @Override
    public void send(FMessage message, boolean important) {
        if (this.isStarted && !this.forceDisconnected) {
            message = message.clone();
            message.setTimeClient(System.currentTimeMillis());
            if (!(message instanceof FPing)) {
                message.setCsSequence(++maxCSSequence);
                message.setScSequence(maxSCSequence);
            }
            if (important) {
                importantQueue.add(message);
            }
            channel.send(message);
        }
    }

    public FFuture send(FMessage message, FFuture future, int timeOut) {
        if (future == FFuture.RECEIVED) {
            message.setResponse(1);
        } else if (future == FFuture.PROCESSED) {
            message.setResponse(2);
        }
        String messageId = UUID.randomUUID().toString();
        message.setMessageId(messageId);
        send(message);
        FFuture fFuture = new FFuture(messageId, timeOut);
        FFutureManager.getInstance().put(messageId, fFuture);
        return fFuture;
    }

    public FFuture send(FMessage message, FFuture future) {
        return send(message, future, 5);
    }

    @Override
    public void setTimeout(int sec) {
    }

    @Override
    public void newSession() {
        LogUtil.info("new session");
        isStarted = true;
        maxCSSequence = 0;
        maxSCSequence = 0;
        importantQueue.clear();
        forceDisconnected = false;

        for (ISessionListener listener : listeners) {
            if (firstSession == 0) {
                listener.onFirstSession(this);
            }
            listener.onNewSession(this);
        }

        firstSession++;
    }

    @Override
    public void start() {
        channel.start();
        scheduler.scheduleAtFixedRate(() -> {
            send(new FPing());
        }, 0, 5, TimeUnit.SECONDS);
    }

    @Override
    public void continueCurrentSession() {
        while (!importantQueue.isEmpty()) {
            FMessage m = importantQueue.poll();
            channel.send(m);
        }
        for (ISessionListener listener : listeners) {
            listener.onContinueCurrentSession(this);
        }
    }

    public void addListener(ISessionListener listener) {
        listeners.add(listener);
    }

    public void removeListener(ISessionListener listener) {
        listeners.remove(listener);
    }

    public void removeAllListeners() {
        listeners.clear();
    }

    public void startThreadQueue() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
