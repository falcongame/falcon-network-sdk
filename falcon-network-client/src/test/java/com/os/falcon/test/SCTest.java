package com.os.falcon.test;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.os.falcon.network.api.message.FMessage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author quanph
 */
public class SCTest extends FMessage {

    private String text;
    private int y;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    
    
    @Override
    public String getEvent() {
        return "s_test";
    }

}
