package com.os.falcon.test;

import com.os.falcon.net.sdk.api.util.LogUtil;
import com.os.falcon.network.api.future.FFuture;
import com.os.falcon.network.api.future.IResponseListener;
import com.os.falcon.network.api.message.CSSCResponse;
import com.os.falcon.network.client.api.common.FClientManager;
import com.os.falcon.network.client.api.session.FChannel;
import com.os.falcon.network.client.api.session.FSession;
import com.os.falcon.network.client.api.session.FSessionManager;
import com.os.falcon.network.client.api.session.ISessionListener;
import com.os.falcon.network.client.socket.netty.FNettyChannel;
import com.os.falcon.network.client.socket.socketio.FSocketIOChannel;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author quanph
 */
public class Main {

    public static void main(String[] args) throws Exception {

        LogUtil.init("conf/falcon_log4j.properties");
//        FChannel channel = new FSocketIOChannel("http://localhost:11112/test");
        FChannel channel = new FNettyChannel("localhost", 11111);
        FSession session = new FSession(channel);
        FSessionManager.getInstance().add(session);

        session.addListener(new ISessionListener() {
            @Override
            public void onChannelConnected(FSession session) {
                LogUtil.info("connected!");
            }

            @Override
            public void onChannelDisconnected(FSession session) {
                LogUtil.info("disconnected!");
            }

            @Override
            public void onTimeout(FSession session) {
            }

            Thread t;
            boolean stopThread = false;

            @Override
            public void onNewSession(FSession session) {

            }

            @Override
            public void onContinueCurrentSession(FSession session) {
            }

            @Override
            public void onFirstSession(FSession session) {

                new Thread(() -> {
                    int i = 0;
                    while (!stopThread) {
                        i++;
                        session.send(new CSTest("Hello " + i, 10));
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }).start();

            }
        });

        session.start();
    }
}
